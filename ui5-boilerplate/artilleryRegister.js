const casual = require("casual");

function generateSignUpData(requestParams, ctx, ee, next){
  ctx.vars["firstName"] = casual.firstName;
  ctx.vars["lastName"] = casual.lastName;
  ctx.vars["email"] = casual.email;
  ctx.vars["password"] = casual.password;
  ctx.vars["city"] = casual.city;
  ctx.vars["countryRegion"] = casual.countryRegion;
  ctx.vars["phoneNumber"] = casual.phoneNumber;
  ctx.vars["lastName"] = casual.lastName;
  ctx.vars["status"] = casual.status;
  
  return next();
}

module.exports = {
    generateSignUpData,
  };