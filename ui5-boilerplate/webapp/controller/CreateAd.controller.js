sap.ui.define(
  [
    'ui5boilerplate/controller/Base.controller',
    'sap/ui/model/json/JSONModel',
    'sap/m/MessageToast',
    'ui5boilerplate/utils/Validations',
    'sap/m/MessageBox',
    'ui5boilerplate/utils/AjaxClient',
    'sap/ui/core/Fragment',
    'ui5boilerplate/utils/Select',

  ],
  (BaseController, JSONModel, MessageToast, Validations, MessageBox, AjaxClient, Fragment, Select) => {
    return BaseController.extend('ui5boilerplate.controller.CreateAd', {
      AjaxClient: AjaxClient,
      getRouter: function () {
        return this.getOwnerComponent().getRouter();
      },
      onInit() {
        const oRouter = this.getOwnerComponent().getRouter();
        oRouter.getRoute("CreateAd").attachMatched(this.CreateAdFunctions, this);

      },

      CreateAdFunctions: function () {
        this.verifyToken();
        this.setupModels();
        this.showBusyIndicator(2000, 0);
      },

      setupModels: function () {

        let deleteAccount = new JSONModel({
          chechBox1: false,
          checkBox2: false,
          checkBox3: false,
          suggestion:""
        });
        this.getView().setModel(deleteAccount, "deleteAccount");

        let stateModel = new JSONModel({
          firstNameState: "None",
          lastNameState: "None",
          cityState: "None",
          phoneNumberState: "None",
          countyState: "None"
        });
        this.getView().setModel(stateModel, "stateModel");


        const id = document.cookie.split(/[\ ;=]/);
        let newRoom = new JSONModel({
          images: [],
          owner: id[id.length - 1],
          mainDescription: 'Casa',
          typeRoom: 'Intregul spatiu',
          countryRegion: 'Afghanistan - AF',
          wifi: false,
          tv: false,
          kitchen: false,
          washer: false,
          freeParking: false,
          airConditioning: false,
          pool: false,
          exerciseEquipment: false,
          smokeAlarm: false,
          firstAidKit: false,
          fireExtinguisher: false,
          securityCamera: false,
          sumRatings: 0,
          totalRatings: 0,
          averageRatings: 1,
          comments: []


        });
        this.getView().setModel(newRoom, "newRoom");
        console.log(newRoom);

        let oData = {
          SelectedCounty: 'UserCounty',
          CountyCollection: [
            { StatusId: 'Alba', Status: 'Alba' },
            { StatusId: 'Arad', Status: 'Arad' },
            { StatusId: 'Arges', Status: 'Arges' },
            { StatusId: 'Bacau', Status: 'Bacau' },
            { StatusId: 'Bihor', Status: 'Bihor' },
            { StatusId: 'Bistrita-Nasaud', Status: 'Bistrita-Nasaud' },
            { StatusId: 'Botosani', Status: 'Botosani' },
            { StatusId: 'Brasov', Status: 'Brasov' },
            { StatusId: 'Braila', Status: 'Braila' },
            { StatusId: 'Bucuresti', Status: 'Bucuresti' },
            { StatusId: 'Buzau', Status: 'Buzau' },
            { StatusId: 'Caras-Severin', Status: 'Caras-Severin' },
            { StatusId: 'Calarasi', Status: 'Calarasi' },
            { StatusId: 'Cluj', Status: 'Cluj' },
            { StatusId: 'Constanta', Status: 'Constanta' },
            { StatusId: 'Covasna', Status: 'Covasna' },
            { StatusId: 'Dambovita', Status: 'Dambovita' },
            { StatusId: 'Dolj', Status: 'Dolj' },
            { StatusId: 'Galati', Status: 'Galati' },
            { StatusId: 'Giurgiu', Status: 'Giurgiu' },
            { StatusId: 'Gorj', Status: 'Gorj' },
            { StatusId: 'Harghita', Status: 'Harghita' },
            { StatusId: 'Hunedoara', Status: 'Hunedoara' },
            { StatusId: 'Ialomita', Status: 'Ialomita' },
            { StatusId: 'Iasi', Status: 'Iasi' },
            { StatusId: 'Ilfov', Status: 'Ilfov' },
            { StatusId: 'Maramures', Status: 'Maramures' },
            { StatusId: 'Mehedinti', Status: 'Mehedinti' },
            { StatusId: 'Mures', Status: 'Mures' },
            { StatusId: 'Neamt', Status: 'Neamt' },
            { StatusId: 'Olt', Status: 'Olt' },
            { StatusId: 'Prahova', Status: 'Prahova' },
            { StatusId: 'Satu Mare', Status: 'Satu Mare' },
            { StatusId: 'Salaj', Status: 'Salaj' },
            { StatusId: 'Sibiu', Status: 'Sibiu' },
            { StatusId: 'Suceava', Status: 'Suceava' },
            { StatusId: 'Teleorman', Status: 'Teleorman' },
            { StatusId: 'Timis', Status: 'Timis' },
            { StatusId: 'Tulcea', Status: 'Tulcea' },
            { StatusId: 'Vaslui', Status: 'Vaslui' },
            { StatusId: 'Valcea', Status: 'Valcea' },
            { StatusId: 'Vrancea', Status: 'Vrancea' },
          ],
          Editable: true,
          Enabled: true,
        };
        let oModel = new JSONModel(oData);
        this.getView().setModel(oModel, 'SelectedModel');

        let dData = {
          SelectedMainDescription: 'MainDescription',
          MainDescriptionCollection: [
            { StatusId: 'Casa', Status: 'Casa' },
            { StatusId: 'Cabana', Status: 'Cabana'},
            { StatusId: 'Apartament', Status: 'Apartament' },
            { StatusId: 'Hambar', Status: 'Hambar' },
            { StatusId: 'Barca', Status: 'Barca' },
            { StatusId: 'Boat', Status: 'Boat' },
            { StatusId: 'Rulota', Status: 'Rulota' },
            { StatusId: 'Casa particulara', Status: 'Casa particulara' },
            { StatusId: 'Castel', Status: 'Castel' },
            { StatusId: 'Pestera', Status: 'Pestera' },
            { StatusId: 'Ferma', Status: 'Ferma' },
            { StatusId: 'Hotel', Status: 'Hotel' },
            { StatusId: 'Cort', Status: 'Cort' },
            { StatusId: 'Casa mica', Status: 'Casa mica' },
            { StatusId: 'Casa in copac', Status: 'Casa in copac' },

          ],
          Editable: true,
          Enabled: true,
        };
        let dModel = new JSONModel(dData);
        this.getView().setModel(dModel, 'SelectedModel1');


        let tData = {
          SelectedTypeRoom: 'TypeRoom',
          TypeRoomCollection: [
            { StatusId: 'Intregul spatiu', Status: 'Intregul spatiu' },
            { StatusId: 'O camera privata', Status: 'O camera privata' },
            { StatusId: 'O camera comuna', Status: 'O camera comuna' },
          ],
          Editable: true,
          Enabled: true,
        };
        let tModel = new JSONModel(tData);
        this.getView().setModel(tModel, 'SelectedModel2');


        let cData = {
          SelectedCountry: 'Country',
          CountryCollection: [
            { StatusId: 'Afghanistan - AF', Status: 'Afghanistan - AF' },
            { StatusId: 'Aland Islands - AX', Status: 'Aland Islands - AX' },
            { StatusId: 'Albania - AL', Status: 'Albania - AL' },
            { StatusId: 'American Samoa - AS', Status: 'American Samoa - AS' },
            { StatusId: 'Andorra - AD', Status: 'Andorra - AD' },
            { StatusId: 'Angola - AO', Status: 'Angola - AO' },
            { StatusId: 'Anguilla - AI', Status: 'Anguilla - AI' },
            { StatusId: 'Antigua & Barbuda - AG', Status: 'Antigua & Barbuda - AG' },
            { StatusId: 'Argentina - AR', Status: 'Argentina - AR' },
            { StatusId: 'Armenia - AM', Status: 'Armenia - AM' },
            { StatusId: 'Aruba - AW', Status: 'Aruba - AW' },
            { StatusId: 'Australia - AU', Status: 'Australia - AU' },
            { StatusId: 'Austria - AT', Status: 'Austria - AT' },
            { StatusId: 'Azerbaijan - AZ', Status: 'Azerbaijan - AZ' },
            { StatusId: 'Bahamas - BS', Status: 'Bahamas - BS' },
            { StatusId: 'Bahrain - BH', Status: 'Bahrain - BH' },
            { StatusId: 'Bangladesh - BD', Status: 'Bangladesh - BD' },
            { StatusId: 'Barbados - BB', Status: 'Barbados - BB' },
            { StatusId: 'Belarus - BY', Status: 'Belarus - BY' },
            { StatusId: 'Belgium - BE', Status: 'Belgium - BE' },
            { StatusId: 'Belize - BZ', Status: 'Belize - BZ' },
            { StatusId: 'Benin - BJ', Status: 'Benin - BJ' },
            { StatusId: 'Bermuda - BM', Status: 'Bermuda - BM' },
            { StatusId: 'Bhutan - BT', Status: 'Bhutan - BT' },
            { StatusId: 'Bolivia - BO', Status: 'Bolivia - BO' },
            { StatusId: 'Bosnia & Herzegovina - BA', Status: 'Bosnia & Herzegovina - BA' },
            { StatusId: 'Botswana - BW', Status: 'Botswana - BW' },
            { StatusId: 'Brazil - BR', Status: 'Brazil - BR' },
            { StatusId: 'British Indian Ocean Territory - IO', Status: 'British Indian Ocean Territory - IO' },
            { StatusId: 'British Virgin Islands - VG', Status: 'British Virgin Islands - VG' },
            { StatusId: 'Brunei - BN', Status: 'Brunei - BN' },
            { StatusId: 'Bulgaria - BG', Status: 'Bulgaria - BG' },
            { StatusId: 'Burkina Faso - BF', Status: 'Burkina Faso - BF' },
            { StatusId: 'Burundi - BI', Status: 'Burundi - BI' },
            { StatusId: 'Cambodia - KH', Status: 'Cambodia - KH' },
            { StatusId: 'Cameroon - CM', Status: 'Cameroon - CM' },
            { StatusId: 'Canada - CA', Status: 'Canada - CA' },
            { StatusId: 'Cape Verde - CV', Status: 'Cape Verde - CV' },
            { StatusId: 'Bonaire, Sint Eustatius and Saba - BQ', Status: 'Bonaire, Sint Eustatius and Saba - BQ' },
            { StatusId: 'Cayman Islands - KY', Status: 'Cayman Islands - KY' },
            { StatusId: 'Central African Republic - CF', Status: 'Central African Republic - CF' },
            { StatusId: 'Chad - TD', Status: 'Chad - TD' },
            { StatusId: 'China - CN', Status: 'China - CN' },
            { StatusId: 'Christmas Island - CX', Status: 'Christmas Island - CX' },
            { StatusId: 'Cocos (Keeling) Islands - CC', Status: 'Cocos (Keeling) Islands - CC' },
            { StatusId: 'Colombia - CO', Status: 'Colombia - CO' },
            { StatusId: 'Comoros - KM', Status: 'Comoros - KM' },
            { StatusId: 'Congo - CG', Status: 'Congo - CG' },
            { StatusId: 'Cook Islands - CK', Status: 'Cook Islands - CK' },
            { StatusId: 'Costa Rica - CR', Status: 'Costa Rica - CR' },
            { StatusId: 'Croatia - HR', Status: 'Croatia - HR' },
            { StatusId: 'Cuba - CU', Status: 'Cuba - CU' },
            { StatusId: 'Curacao - CW', Status: 'Curacao - CW' },
            { StatusId: 'Cyprus - CY', Status: 'Cyprus - CY' },
            { StatusId: 'Czechia - CZ', Status: 'Czechia - CZ' },
            { StatusId: 'Democratic Republic of the Congo - CD', Status: 'Democratic Republic of the Congo - CD' },
            { StatusId: 'Denmark - DK', Status: 'Denmark - DK' },
            { StatusId: 'Djibouti - DJ', Status: 'Djibouti - DJ' },
            { StatusId: 'Dominica - DM', Status: 'Dominica - DM' },
            { StatusId: 'Dominican Republic - DO', Status: 'Dominican Republic - DO' },
            { StatusId: 'Timor-Leste - TL', Status: 'Timor-Leste - TL' },
            { StatusId: 'Ecuador - EC', Status: 'Ecuador - EC' },
            { StatusId: 'Egypt - EG', Status: 'Egypt - EG' },
            { StatusId: 'El Salvador - SV', Status: 'El Salvador - SV' },
            { StatusId: 'Equatorial Guinea - GQ', Status: 'Equatorial Guinea - GQ' },
            { StatusId: 'Eritrea - EE', Status: 'Eritrea - EE' },
            { StatusId: 'Ethiopia - ET', Status: 'Ethiopia - ET' },
            { StatusId: 'Falkland Islands(Islas Malvinas) - FK', Status: 'Falkland Islands(Islas Malvinas) - FK' },
            { StatusId: 'Feroe Islands - FO', Status: 'Feroe Islands - FO' },
            { StatusId: 'Fiji - FJ', Status: 'Fiji - FJ' },
            { StatusId: 'Finland - FI', Status: 'Finland- FI' },
            { StatusId: 'France - FR', Status: 'France - FR' },
            { StatusId: 'French Polynesia - PF', Status: 'French Polynesia - PF' },
            { StatusId: 'Gabon - GA', Status: 'Gabon - GA' },
            { StatusId: 'Gambia - GM', Status: 'Gambia - GM' },
            { StatusId: 'Georgia - GE', Status: 'Georgia - GE' },
            { StatusId: 'Germany - DE', Status: 'Germany - DE' },
            { StatusId: 'Ghana - GH', Status: 'Ghana - GH' },
            { StatusId: 'Gibraltar - GI', Status: 'Gibraltar - GI' },
            { StatusId: 'Greece - GR', Status: 'Greece - GR' },
            { StatusId: 'Greenland - GL', Status: 'Greenland - GL' },
            { StatusId: 'Grenada - GD', Status: 'Grenada - GD' },
            { StatusId: 'Guadeloupe - GP', Status: 'Guadeloupe - GP' },
            { StatusId: 'Guam - GU', Status: 'Guam - GU' },
            { StatusId: 'Guatemala - GT', Status: 'Guatemala - GT' },
            { StatusId: 'Guernsey - GG', Status: 'Guernsey - GG' },
            { StatusId: 'Guinea - GN', Status: 'Guinea - GN' },
            { StatusId: 'Guinea-Bissau - GW', Status: 'Guinea-Bissau - GW' },
            { StatusId: 'Guyana - GY', Status: 'Guyana - GY' },
            { StatusId: 'Haiti - HT', Status: 'Haiti - HT' },
            { StatusId: 'Honduras - HN', Status: 'Honduras - HN' },
            { StatusId: 'Hong Kong - HK', Status: 'Hong Kong - HK' },
            { StatusId: 'Hungary - HU', Status: 'Hungary - HU' },
            { StatusId: 'Iceland - IS', Status: 'Iceland - IS' },
            { StatusId: 'India - IN', Status: 'India - IN' },
            { StatusId: 'Indonesia - ID', Status: 'Indonesia - ID' },
            { StatusId: 'Iraq - IQ', Status: 'Iraq - IQ' },
            { StatusId: 'Ireland - IE', Status: 'Ireland - IE' },
            { StatusId: 'Isle of Man - IM', Status: 'Isle of Man - IM' },
            { StatusId: 'Israel - IL', Status: 'Israel - IL' },
            { StatusId: 'Italy - IT', Status: 'Italy - IT' },
            { StatusId: 'Cote de Ivoire - CI', Status: 'Cote de Ivoire - CI' },
            { StatusId: 'Jamaica - KM', Status: 'Jamaica - KM' },
            { StatusId: 'Japan - JP', Status: 'Japan - JP' },
            { StatusId: 'Jersey - JE', Status: 'Jersey - JE' },
            { StatusId: 'Jordan - JO', Status: 'Jordan - JO' },
            { StatusId: 'Kazakhstan - KZ', Status: 'Kazakhstan - KZ' },
            { StatusId: 'Kenya - KE', Status: 'Kenya - KE' },
            { StatusId: 'Kiribati - KI', Status: 'Kiribati - KI' },
            { StatusId: 'Kosovo - XK', Status: 'Kosovo - XK' },
            { StatusId: 'Kuwait - KW', Status: 'Kuwait - KW' },
            { StatusId: 'Kyrgyzstan - KG', Status: 'Kyrgyzstan - KG' },
            { StatusId: 'Laos - LA', Status: 'Laos - LA' },
            { StatusId: 'Latvia - LV', Status: 'Latvia - LV' },
            { StatusId: 'Lebanon - LB', Status: 'Lebanon - LB' },
            { StatusId: 'Lesotho - LS', Status: 'Lesotho - LS' },
            { StatusId: 'Liberia - LR', Status: 'Liberia - LR' },
            { StatusId: 'Libya - LY', Status: 'Libya - LY' },
            { StatusId: 'Liechtenstein - LI', Status: 'Liechtenstein - LI' },
            { StatusId: 'Lithuania - LT', Status: 'Lithuania - LT' },
            { StatusId: 'Luxembourg - LU', Status: 'Luxembourg - LU' },
            { StatusId: 'Macau - MO', Status: 'Macau - MO' },
            { StatusId: 'North Macedonia - MK', Status: 'North Macedonia - MK' },
            { StatusId: 'Madagascar - MG', Status: 'Madagascar - MG' },
            { StatusId: 'Malawi - MW', Status: 'Malawi - MW' },
            { StatusId: 'Malaysia - MY', Status: 'Malaysia - MY' },
            { StatusId: 'Maldives - MV', Status: 'Maldives - MV' },
            { StatusId: 'Mali - ML', Status: 'Mali - ML' },
            { StatusId: 'Malta - MT', Status: 'Malta - MT' },
            { StatusId: 'Marshall Islands - MH', Status: 'Marshall Islands - MH' },
            { StatusId: 'Martinique - MQ', Status: 'Martinique - MQ' },
            { StatusId: 'Mauritania - MR', Status: 'Mauritania - MR' },
            { StatusId: 'Mauritius - MU', Status: 'Mauritius - MU' },
            { StatusId: 'Mayotte - YT', Status: 'Mayotte - YT' },
            { StatusId: 'Mexico - MX', Status: 'Mexico - MX' },
            { StatusId: 'Micronesia - FM', Status: 'Micronesia - FM' },
            { StatusId: 'Moldova - MD', Status: 'Moldova - MD' },
            { StatusId: 'Monaco - MC', Status: 'Monaco - MC' },
            { StatusId: 'Mongolia - MN', Status: 'Mongolia - MN' },
            { StatusId: 'Montenegro - ME', Status: 'Montenegro - ME' },
            { StatusId: 'Montserrat - MS', Status: 'Montserrat - MS' },
            { StatusId: 'Morocco - MA', Status: 'Morocco - MA' },
            { StatusId: 'Mozambique - MZ', Status: 'Mozambique - MZ' },
            { StatusId: 'Myanmar - MM', Status: 'Myanmar - MM' },
            { StatusId: 'Namibia - NA', Status: 'Namibia - NA' },
            { StatusId: 'Nauru - NR', Status: 'Nauru - NR' },
            { StatusId: 'Nepal - NP', Status: 'Nepal - NP' },
            { StatusId: 'Netherlands - NL', Status: 'Netherlands - NL' },
            { StatusId: 'New Caledonia - NC', Status: 'New Caledonia - NC' },
            { StatusId: 'New Zeakand - NZ', Status: 'New Zeakand - NZ' },
            { StatusId: 'Nicaragua - NI', Status: 'Nicaragua - NI' },
            { StatusId: 'Niger - NE', Status: 'Niger - NE' },
            { StatusId: 'Nigeria - NG', Status: 'Nigeria - NG' },
            { StatusId: 'Niue - NU', Status: 'Niue - NU' },
            { StatusId: 'Norfolk Island - NF', Status: 'Norfolk Island - NF' },
            { StatusId: 'Northern Mariana Islands - MP', Status: 'Northern Mariana Islands - MP' },
            { StatusId: 'Norway - NO', Status: 'Norway - NO' },
            { StatusId: 'Oman - OM', Status: 'Oman - OM' },
            { StatusId: 'Pakistan - PK', Status: 'Pakistan - PK' },
            { StatusId: 'Palau - PW', Status: 'Palau - PW' },
            { StatusId: 'Palestinian Territories - PS', Status: 'Palestinian Territories - PS' },
            { StatusId: 'Panama - PA', Status: 'Panama - PA' },
            { StatusId: 'Papua New Guinea - PG', Status: 'Papua New Guinea - PG' },
            { StatusId: 'Paraguay - PY', Status: 'Paraguay - PY' },
            { StatusId: 'Peru - PE', Status: 'Peru - PE' },
            { StatusId: 'Philippines - PH', Status: 'Philippines - PH' },
            { StatusId: 'Pitcairn Islands - PN', Status: 'Pitcairn Islands - PN' },
            { StatusId: 'Poland - PL', Status: 'Poland - PL' },
            { StatusId: 'Portugal - PT', Status: 'Portugal - PT' },
            { StatusId: 'Puerto Rico - PR', Status: 'Puerto Rico - PR' },
            { StatusId: 'Qatar - QA', Status: 'Qatar - QA' },
            { StatusId: 'Reunion - RE', Status: 'Reunion - RE' },
            { StatusId: 'Romania - RO', Status: 'Romania - RO' },
            { StatusId: 'Russia - RU', Status: 'Russia - RU' },
            { StatusId: 'Rwanda - RW', Status: 'Rwanda - RW' },
            { StatusId: 'St. Barthelemy - BL', Status: 'St. Barthelemy - BL' },
            { StatusId: 'St. Helena - SH', Status: 'St. Helena - SH' },
            { StatusId: 'St. Kitts & Nevis - KN', Status: 'St. Kitts & Nevis - KN' },
            { StatusId: 'St. Lucia - LC', Status: 'St. Lucia - LC' },
            { StatusId: 'St. Martin - MF', Status: 'St. Martin - MF' },
            { StatusId: 'St. Pierre & Miquelon - PM', Status: 'St. Pierre & Miquelon - PM' },
            { StatusId: 'St. Vincent & Grenadines - VC', Status: 'St. Vincent & Grenadines - VC' },
            { StatusId: 'Samoa - WS', Status: 'Samoa - WS' },
            { StatusId: 'San Marino - SM', Status: 'San Marino - SM' },
            { StatusId: 'Sao Tome & Principe - ST', Status: 'Sao Tome & Principe - ST' },
            { StatusId: 'Saudi Arabia - SA', Status: 'Saudi Arabia - SA' },
            { StatusId: 'Senegal - SN', Status: 'Senegal - SN' },
            { StatusId: 'Serbia - RS', Status: 'Serbia - RS' },
            { StatusId: 'Seychelles - SC', Status: 'Seychelles - SC' },
            { StatusId: 'Sierra Leone - SL', Status: 'Sierra Leone - SL' },
            { StatusId: 'Singapore - SG', Status: 'Singapore - SG' },
            { StatusId: 'Sint Maarten - SX', Status: 'Sint Maarten - SX' },
            { StatusId: 'Slovakia - SK', Status: 'Slovakia - SK' },
            { StatusId: 'Slovenia - SI', Status: 'Slovenia - SI' },
            { StatusId: 'Solomon Islands - SB', Status: 'Solomon Islands - SB' },
            { StatusId: 'Somalia - SO', Status: 'Somalia - SO' },
            { StatusId: 'South Africa - ZA', Status: 'South Africa - ZA' },
            { StatusId: 'South Georgia & South Sandwich Islands - GS', Status: 'South Georgia & South Sandwich Islands - GS' },
            { StatusId: 'South Korea - KR', Status: 'South Korea - KR' },
            { StatusId: 'South Sudan - SS', Status: 'South Sudan - SS' },
            { StatusId: 'Spain - ES', Status: 'Spain - ES' },
            { StatusId: 'Sri Lanka - LK', Status: 'Sri Lanka - LK' },
            { StatusId: 'Sudan - SD', Status: 'Sudan - SD' },
            { StatusId: 'Suriname - SR', Status: 'Suriname - SR' },
            { StatusId: 'Svalbard & Jan Mayen - SJ', Status: 'Svalbard & Jan Mayen - SJ' },
            { StatusId: 'Eswatini - SZ', Status: 'Eswatini - SZ' },
            { StatusId: 'Sweden - SE', Status: 'Sweden - SE' },
            { StatusId: 'Switzerland - CH', Status: 'Switzerland - CH' },
            { StatusId: 'Taiwan - TW', Status: 'Taiwan - TW' },
            { StatusId: 'Tajikistan - TJ', Status: 'Tajikistan - TJ' },
            { StatusId: 'Tanzania - TZ', Status: 'Tanzania - TZ' },
            { StatusId: 'Thailand - TH', Status: 'Thailand - TH' },
            { StatusId: 'Togo - TG', Status: 'Togo - TG' },
            { StatusId: 'Tokelau - TK', Status: 'Tokelau - TK' },
            { StatusId: 'Tonga - TO', Status: 'Tonga - TO' },
            { StatusId: 'Trinidad & Tobago - TT', Status: 'Trinidad & Tobago - TT' },
            { StatusId: 'Tunisia - TN', Status: 'Tunisia - TN' },
            { StatusId: 'Turkey - TR', Status: 'Turkey - TR' },
            { StatusId: 'Turkmenistan - TM', Status: 'Turkmenistan - TM' },
            { StatusId: 'Turks & Caicos Islands - TC', Status: 'Turks & Caicos Islands - TC' },
            { StatusId: 'Tuvalu - TV', Status: 'Tuvalu - TV' },
            { StatusId: 'U.S Virgin Islands - VI', Status: 'U.S Virgin Islands - VI' },
            { StatusId: 'Uganda - UG', Status: 'Uganda - UG' },
            { StatusId: 'Ukraine - UA', Status: 'Ukraine - UA' },
            { StatusId: 'United Arab Emirates - AE', Status: 'United Arab Emirates - AE' },
            { StatusId: 'United Kingdom - GB', Status: 'United Kingdom - GB' },
            { StatusId: 'United States - US', Status: 'United States - US' },
            { StatusId: 'Uruguay - UY', Status: 'Uruguay - UY' },
            { StatusId: 'Uzbekistan - UZ', Status: 'Uzbekistan - UZ' },
            { StatusId: 'Vanuatu - VU', Status: 'Vanuatu - VU' },
            { StatusId: 'Vatican City - VA', Status: 'Vatican City - VA' },
            { StatusId: 'Venezuela - VE', Status: 'Venezuela - VE' },
            { StatusId: 'Vietnam - VN', Status: 'Vietnam - VN' },
            { StatusId: 'Wallis & Futuna - WF', Status: 'Wallis & Futuna - WF' },
            { StatusId: 'Western Sahara - EH', Status: 'Western Sahara - EH' },
            { StatusId: 'Yemen - YE', Status: 'Yemen - YE' },
            { StatusId: 'Zambia - ZM', Status: 'Zambia - ZM' },
            { StatusId: 'Zimbabwe - ZW', Status: 'Zimbabwe - ZM' },
          ],
          Editable: true,
          Enabled: true,
        };
        let cModel = new JSONModel(cData);
        this.getView().setModel(cModel, 'SelectedModel3');
      },

      checkTitle: function () {
        let roomCheck = this.getView().getModel('newRoom').getData();
        let title = roomCheck.title;
        if (Validations.validateNoSpace(title)) {
          this.getView().getModel('newRoom').setProperty('/titleState', 'None');
        } else {
          this.getView().getModel('newRoom').setProperty('/titleState', 'Error');
        }
      },
      checkStreet: function () {
        let roomCheck = this.getView().getModel('newRoom').getData();
        let street = roomCheck.street;
        if (Validations.validateNoSpace(street)) {
          this.getView().getModel('newRoom').setProperty('/streetState', 'None');
        } else {
          this.getView().getModel('newRoom').setProperty('/streetState', 'Error');
        }
      },

      checkNrApt: function () {
        let roomCheck = this.getView().getModel('newRoom').getData();
        let nrApt = roomCheck.nrApt;
        if (Validations.validateNumber(nrApt)) {
          this.getView().getModel('newRoom').setProperty('/nrAptState', 'None');
        } else {
          this.getView().getModel('newRoom').setProperty('/nrAptState', 'Error');
        }
      },

      checkCity: function () {
        let roomCheck = this.getView().getModel('newRoom').getData();
        let city = roomCheck.city;
        if (Validations.validateName(city)) {
          this.getView().getModel('newRoom').setProperty('/cityState', 'None');
        } else {
          this.getView().getModel('newRoom').setProperty('/cityState', 'Error');
        }
      },

      checkState: function () {
        let roomCheck = this.getView().getModel('newRoom').getData();
        let state = roomCheck.state;
        if (Validations.validateName(state)) {
          this.getView().getModel('newRoom').setProperty('/stateState', 'None');
        } else {
          this.getView().getModel('newRoom').setProperty('/stateState', 'Error');
        }
      },

      checkZipCode: function () {
        let roomCheck = this.getView().getModel('newRoom').getData();
        let zipCode = roomCheck.zipCode;
        if (Validations.validateNumber(zipCode)) {
          this.getView().getModel('newRoom').setProperty('/zipCodeState', 'None');
        } else {
          this.getView().getModel('newRoom').setProperty('/zipCodeState', 'Error');
        }
      },

      checkDescription: function () {
        let roomCheck = this.getView().getModel('newRoom').getData();
        let description = roomCheck.description;
        if (Validations.validateNoSpace(description)) {
          this.getView().getModel('newRoom').setProperty('/descriptionState', 'None');
        } else {
          this.getView().getModel('newRoom').setProperty('/descriptionState', 'Error');
        }
      },

      checkPrice: function () {
        let roomCheck = this.getView().getModel('newRoom').getData();
        let price = roomCheck.price;
        if (Validations.validateNumber(price)) {
          this.getView().getModel('newRoom').setProperty('/priceState', 'None');
        } else {
          this.getView().getModel('newRoom').setProperty('/priceState', 'Error');
        }
      },

      checkOwnerDescription: function () {
        let roomCheck = this.getView().getModel('newRoom').getData();
        let ownerDescription = roomCheck.ownerDescription;
        if (Validations.validateNoSpace(ownerDescription)) {
          this.getView().getModel('newRoom').setProperty('/ownerDescriptionState', 'None');
        } else {
          this.getView().getModel('newRoom').setProperty('/ownerDescriptionState', 'Error');
        }
      },

      checkRules: function () {
        let roomCheck = this.getView().getModel('newRoom').getData();
        let rules = roomCheck.rules;
        if (Validations.validateNoSpace(rules)) {
          this.getView().getModel('newRoom').setProperty('/rulesState', 'None');
        } else {
          this.getView().getModel('newRoom').setProperty('/rulesState', 'Error');
        }
      },

      checkYlabel: function () {
        let roomCheck = this.getView().getModel('newRoom').getData();
        let yLabel = roomCheck.yLabel;
        if (Validations.validateNoSpace(yLabel)) {
          this.getView().getModel('newRoom').setProperty('/yLabelState', 'None');
        } else {
          this.getView().getModel('newRoom').setProperty('/yLabelState', 'Error');
        }
      },

      checkXlabel: function () {
        let roomCheck = this.getView().getModel('newRoom').getData();
        let xLabel = roomCheck.xLabel;
        if (Validations.validateNoSpace(xLabel)) {
          this.getView().getModel('newRoom').setProperty('/xLabelState', 'None');
        } else {
          this.getView().getModel('newRoom').setProperty('/xLabelState', 'Error');
        }
      },
      

      handleValueChange: function (oEvent) {
        const fileObj = oEvent.getParameter("files");   //array cu toate obiectele fisierele  
        const keys = Object.keys(fileObj);
        this.aSelectedImages = [];
        for (let i = 0; i < keys.length; i++) {
          this.aSelectedImages.push(fileObj[keys[i]]);
        }
        sap.m.MessageBox.information(this.getI18nMessage("PRESS_UPLOAD"));
      },

      handleTypeMissmatch: function (oEvent) {
        var aFileTypes = oEvent.getSource().getFileType();
        aFileTypes.map(function (sType) {
          return "*." + sType;
        });
        sap.m.MessageBox.error(
          this.getI18nMessage("THE_FILE_TYPE") +
          oEvent.getParameter("fileType") + " " +
          this.getI18nMessage("SUPPORTED_FILES") +
          aFileTypes.join(", ") + "."
        );
      },

      handleUploadPress: async function () {

        let formData = new FormData();
        for (let i = 0; i < this.aSelectedImages.length; i++) {
          formData.append("files[]", this.aSelectedImages[i]);
        }
        const data = await jQuery.ajax({
          type: "POST",
          url: "http://localhost:3000/images",
          cache: false,
          contentType: false,
          processData: false,
          enctype: "multipart/form-data",
          data: formData,
          xhrFields: {
            withCredentials: true,
          },
          success: (data) => {
            MessageToast.show(this.getI18nMessage("UPLOAD_SUCCESSFUL"));
            return data;
          },
          error: (err) => {
            MessageToast.show(err);
          },
        });
        this.getView().getModel("newRoom").setProperty("/images", data.map((item) => item.id));
      },

      onApplyPress: function (oEvent) {
        const oData = this.getView().getModel("newRoom").oData;
        const title = oData.title;
        const street = oData.street;
        const nrApt = oData.nrApt;
        const city = oData.city;
        const state = oData.state;
        const zipCode = oData.zipCode;
        const description = oData.description;
        const price = oData.price;
        const ownerDescription = oData.ownerDescription;
        const rules = oData.rules;
        const yLabel = oData.yLabel;
        const xLabel = oData.xLabel;

        console.log(oData);

        if (Validations.validateNoSpace(title) && Validations.validateNoSpace(street) && Validations.validateNumber(nrApt) && Validations.validateName(city) && Validations.validateNoSpace(state) && Validations.validateNumber(zipCode) && Validations.validateNoSpace(description) && Validations.validateNumber(price) && Validations.validateNoSpace(ownerDescription) && Validations.validateNoSpace(rules) && Validations.validateNoSpace(yLabel) && Validations.validateNoSpace(xLabel)) {
          if (!this.kkDialog) {
            this.kkDialog = Fragment.load({
              id: this.getView().getId(),
              name: 'ui5boilerplate.view.CreateAdDialog',
              controller: this,
            }).then((llDialog) => {
              this.getView().addDependent(llDialog);
              return llDialog;
            });
          }
          this.kkDialog.then(function (ooDialog) {
            ooDialog.open();
          });
          this.getView().getModel("newRoom").setProperty("/titleState", "None");
          this.getView().getModel("newRoom").setProperty("/streetState", "None");
          this.getView().getModel("newRoom").setProperty("/nrAptState", "None");
          this.getView().getModel("newRoom").setProperty("/cityState", "None");
          this.getView().getModel("newRoom").setProperty("/stateState", "None");
          this.getView().getModel("newRoom").setProperty("/zipCodeState", "None");
          this.getView().getModel("newRoom").setProperty("/descriptionState", "None");
          this.getView().getModel("newRoom").setProperty("/priceState", "None");
          this.getView().getModel("newRoom").setProperty("/ownerDescriptionState", "None");
          this.getView().getModel("newRoom").setProperty("/rulesState", "None");
          this.getView().getModel("newRoom").setProperty("/xLabelState", "None");
          this.getView().getModel("newRoom").setProperty("/yLabelState", "None");
        }
        else {

          sap.m.MessageBox.error(this.getI18nMessage("HIGHLIGHTED_FIELDS"));
          if (!Validations.validateNoSpace(title) || !title) {
            this.getView().getModel("newRoom").setProperty("/titleState", "Error");
          }
          if (!Validations.validateNoSpace(street) || !street) {
            this.getView().getModel("newRoom").setProperty("/streetState", "Error");
          }
          if (!Validations.validateNumber(nrApt)) {
            this.getView().getModel("newRoom").setProperty("/nrAptState", "Error");
          }
          if (!Validations.validateNoSpace(state) || !state) {
            this.getView().getModel("newRoom").setProperty("/stateState", "Error");
          }
          if (!Validations.validateNumber(zipCode)) {
            this.getView().getModel("newRoom").setProperty("/zipCodeState", "Error");
          }
          if (!Validations.validateNoSpace(description) || !description) {
            this.getView().getModel("newRoom").setProperty("/descriptionState", "Error");
          }
          if (!Validations.validateNumber(price)) {
            this.getView().getModel("newRoom").setProperty("/priceState", "Error");
          }
          if (!Validations.validateNoSpace(ownerDescription) || !ownerDescription) {
            this.getView().getModel("newRoom").setProperty("/ownerDescriptionState", "Error");
          }
          if (!Validations.validateNoSpace(rules) || !rules) {
            this.getView().getModel("newRoom").setProperty("/rulesState", "Error");
          }
          if (!Validations.validateName(city) || !city) {
            this.getView().getModel("newRoom").setProperty("/cityState", "Error");
          }
          if(!Validations.validateNoSpace(xLabel) || !xLabel){
            this.getView().getModel("newRoom").setProperty("/xLabelState", "Error");
          }
          if(!Validations.validateNoSpace(yLabel) || !yLabel){
            this.getView().getModel("newRoom").setProperty("/yLabelState", "Error");
          }
        }

      },

      onCancelAddDialog: function () {
        this.byId("CreateAdDialog").close();
      },

      onAddAccepted: async function () {
        const response = this.post("http://localhost:3000/room", this.getView().getModel("newRoom").oData)
          .then((data) => {
            let newRoom = new JSONModel();
            this.getView().setModel(newRoom, "newRoom");
            MessageToast.show(this.getI18nMessage("ADDED_SUCCESSFULLY"));
            let oRouter = this.getOwnerComponent().getRouter();
            oRouter.navTo("home");
            return data;
          })
          .catch((err) => {
            sap.m.MessageBox.error(this.getI18nMessage("MISSING_INFORMATION"));
            this.byId("CreateAdDialog").close();
            console.log("Post err", err);
            return false;
          })
      },
    });
  }
);
