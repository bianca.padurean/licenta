sap.ui.define(
  [
    'ui5boilerplate/controller/Base.controller',
    'sap/ui/model/json/JSONModel',
    'sap/m/MessageToast',
    'ui5boilerplate/utils/Validations',
    'sap/m/MessageBox',
    "sap/ui/core/Fragment"
  ],
  (BaseController, JSONModel, MessageToast, Validations, MessageBox, Fragment) => {
    return BaseController.extend('ui5boilerplate.controller.RoomPage', {
      onInit() {
        const oRouter = this.getOwnerComponent().getRouter();
        oRouter.getRoute("RoomPage").attachMatched(this.RoomPageFunction, this);

        this.getView().byId("ratingIndicator").setValue(0);
      },

      onAfterRendering: function () {
        this.onChangePage();
        
      },

      onChangePage: function () {
        var oCarousel = this.byId('roomCarousel');
        setTimeout(function () {
          oCarousel.next();
        }, 5000);
      },

      RoomPageFunction: function (oEvent) {
        this.verifyToken();
        this.showBusyIndicator(2000, 0);
        this.byId("roomPage").scrollTo(0);
        
        let stateModel = new JSONModel({
          firstNameState: "None",
          lastNameState: "None",
          cityState: "None",
          phoneNumberState: "None",
          countyState: "None",
        });
        this.getView().setModel(stateModel, "stateModel");

        let deleteAccount = new JSONModel({
          chechBox1: false,
          checkBox2: false,
          checkBox3: false,
          suggestion:""
        });
        this.getView().setModel(deleteAccount, "deleteAccount");

        this.byId("heartToggle").setType("Default");
        this.onVerifyHeart();

        let uploadDateModel = new JSONModel();
        this.getView().setModel(uploadDateModel, "uploadDateModel");

        this.get(
          "http://localhost:3000/room/" +
          oEvent.getParameter("arguments").selectedId
        )
          .then((response) => {
            this.getView().getModel("selectedRoom").setData(response);
            this.setOwnerModel();
            let uploadDate = this.getView()
              .getModel("selectedRoom")
              .getData()
              .createdAt.split("T")[0];
            this.getView().getModel("uploadDateModel").setData(uploadDate);
          })
          .catch((err) => {
            console.log(err);
          });

      },

      setOwnerModel() {
        let ownerId = this.getOwnerComponent()
          .getModel("selectedRoom")
          .getData().owner;
        this.get("http://localhost:3000/user/" + ownerId)
          .then((data) => {
            let ownerModel = new JSONModel(data);
            this.getView().setModel(ownerModel, "ownerModel");
          })
          .catch((err) => {
            console.log(err);
          });
      },

      onPressShowSeller: function (oEvent) {
        let oButton = oEvent.getSource();
        let oViewSeller = this.getView();

        if (!this._sPopover) {
          this._sPopover = Fragment.load({
            id: "sellerPopover",
            name: "ui5boilerplate.view.ContactOwnerDialog",
            controller: this,
          }).then((osPopover) => {
            oViewSeller.addDependent(osPopover);
            return osPopover;
          });
        }
        this._sPopover.then(function (osPopover) {
          osPopover.openBy(oButton);
        });
      },

      onVerifyHeart: function () {
        const userId = this.getUserId();
        let selectedAd = this.getView().getModel("selectedRoom").getData()._id;
        this.get("http://localhost:3000/user/" + userId)
          .then((data) => {
            data.favourites.forEach((element) => {
              if (element == selectedAd) {
                this.byId("heartToggle").setType("Reject");
              }
            });
          })
          .catch((err) => {
            console.log(err);
          });
      },

      onPressHeart: function () {
        let value = this.byId("heartToggle").getType();
        if (value == "Default") {
          let selectedAd = this.getView().getModel("selectedRoom").getData()._id;
          this.get("http://localhost:3000/userr/favourites")
            .then((data) => {
              this.addToFavourites(selectedAd);
              this.byId("heartToggle").setType("Reject")
            })
            .catch((err) => {
              console.log(err);
            });
        }
        else {
          let id = this.getView().getModel("selectedRoom").getData()._id;
          this.delete("http://localhost:3000/fav/" + id)
            .then((response) => {
              this.byId("heartToggle").setType("Default")
              MessageToast.show(this.getI18nMessage("REMOVE_FAVOURITESPOST"));
            })
            .catch((err) => {
              console.log("err", err);
            });
        }
      },

      addToFavourites: function (selectedAd) {
        this.post(`http://localhost:3000/add-to-favourites/${selectedAd}`)
          .then((data) => {
            let favouriteRooms = new JSONModel(data);
            this.getOwnerComponent().setModel(favouriteRooms, "favouriteRooms");
            MessageToast.show(this.getI18nMessage("SUCCESS_FAVOURITES"));
          })
          .catch((err) => {
            console.log(err);
          });
      },

      onPressHome: function (evt) {
        let oRouter = this.getOwnerComponent().getRouter();
        oRouter.navTo('home');
      },

      onPressMap: function (oEvent){
      this.get(
        "http://localhost:3000/room/" +
        this.getView().getModel("selectedRoom").getData()._id
      )
        .then((response) => {
          let selectedMapId = this.getView()
            .getModel("selectedRoom")
            .getData()._id;
          this.getRouter().navTo("Map", { selectedId: selectedMapId });
        })
        .catch((err) => {
          console.log(err);
        });
      }
    });
  }
);