sap.ui.define(['ui5boilerplate/controller/Base.controller', 'sap/ui/core/Fragment', 'ui5boilerplate/utils/Validations', 'ui5boilerplate/utils/AjaxClient', 'sap/ui/model/json/JSONModel', 'sap/m/MessageToast', "sap/m/MessageBox", "sap/ui/model/Filter", "sap/ui/model/FilterOperator"], (BaseController, Fragment, Validations, AjaxClient, JSONModel, MessageToast, MessageBox, Filter, FilterOperator) => {
  return BaseController.extend('ui5boilerplate.controller.Home', {
    AjaxClient: AjaxClient,
    onInit() {

      const oRouter = this.getOwnerComponent().getRouter();
      oRouter.getRoute("home").attachMatched(this.LandingPageFunction, this);


      this._filters = {};

      let deleteAccount = new JSONModel({
        chechBox1: false,
        checkBox2: false,
        checkBox3: false,
        suggestion:""
      });
      this.getView().setModel(deleteAccount, "deleteAccount");
    },


    LandingPageFunction() {
      this.verifyToken();
      this.onShowAll();
      this.onClearFilters();

      let stateModel = new JSONModel({
        firstNameState: "None",
        lastNameState: "None",
        cityState: "None",
        phoneNumberState: "None",
        countyState: "None"
      });
      this.getView().setModel(stateModel, "stateModel");

      const cookieData = document.cookie.split(" ");
      const id = cookieData[1].split("=")[1];

      this.get("http://localhost:3000/user/" + id)
        .then((data) => {
          let userModel = new JSONModel(data);
          this.getView().setModel(userModel, "userModel");

        })
        .catch((err) => {
          console.log(err);
        });
    },

    onShowAll() {
      this.byId("adsGridList").setVisible(true);
      this.get(
        "http://localhost:3000/roo",
      )
        .then((data) => {
          let roomAds = new JSONModel(data);
          this.getView().setModel(roomAds, "roomAds");
          console.log(roomAds);
        })
        .catch((err) => {
          console.log(err);
        });
    },

    onSearchData: function (oEvent) {
      var aFilter = [];
      var sQuery = oEvent.getParameter("query");
      if (sQuery) {
        aFilter.push(new Filter("title", FilterOperator.Contains, sQuery));
      }
      var oList = this.byId("adsGridList");
      var oBinding = oList.getBinding("items");
      oBinding.filter(aFilter);
    },

    onCountryChange: function (oEvent) {
      const selectedCountry = oEvent.getSource().getValue();
      console.log(selectedCountry);
      this._filters.country = new Filter("countryRegion", FilterOperator.Contains, selectedCountry);
      this.applyFiltersToTable();
    },

    onMainDescriptionChange: function (oEvent) {
      const selectedDescription = oEvent.getSource().getValue();
      console.log(selectedDescription);
      this._filters.mainDescription = new Filter("mainDescription", FilterOperator.Contains, selectedDescription);
      this.applyFiltersToTable();
    },

    onTypeRoomChange: function (oEvent) {
      const selectedTypeRoom = oEvent.getSource().getValue();
      console.log(selectedTypeRoom);
      this._filters.mainDescription = new Filter("typeRoom", FilterOperator.Contains, selectedTypeRoom);
      this.applyFiltersToTable();
    },

    onBedroomsChange: function (oEvent) {
      const selectedBedrooms = oEvent.getSource().getValue();
      console.log(selectedBedrooms);
      this._filters.bedrooms = new Filter("bedrooms", FilterOperator.Contains, selectedBedrooms);
      this.applyFiltersToTable();
    },

    onBedsChange: function (oEvent) {
      const selectedBeds = oEvent.getSource().getValue();
      console.log(selectedBeds);
      this._filters.beds = new Filter("beds", FilterOperator.Contains, selectedBeds);
      this.applyFiltersToTable();
    },

    onBathroomsChange: function (oEvent) {
      const selectedBathrooms = oEvent.getSource().getValue();
      console.log(selectedBathrooms);
      this._filters.bathrooms = new Filter("bathrooms", FilterOperator.Contains, selectedBathrooms);
      this.applyFiltersToTable();
    },

    onPriceMinChange: function (oEvent) {
      const selectedPriceMin = oEvent.getSource().getValue();
      console.log(selectedPriceMin);
      this._filters.priceMin = new Filter("price", FilterOperator.GE, selectedPriceMin);
      this.applyFiltersToTable();
    },

    onPriceMaxChange: function (oEvent) {
      const selectedPriceMax = oEvent.getSource().getValue();
      console.log(selectedPriceMax);
      this._filters.priceMax = new Filter("price", FilterOperator.LE, selectedPriceMax);
      this.applyFiltersToTable();

      if (selectedPriceMax == "") {
        this.onShowAll();
        this._filters = {};
      }
    },

    applyFiltersToTable: function () {
      const dataTable = this.getView().byId("adsGridList");
      this.aFilters = [];
      for (const property in this._filters) {
        this.aFilters.push(this._filters[property])
      }
      const filters = new Filter({
        aFilters: this.aFilters,
        bAnd: true
      })
      dataTable.getBinding("items").filter(filters);
    },

    onClearFilters() {
      this.onShowAll();

      this.byId("countryRegionCombo").clearSelection();
      this.byId("mainDescriptionCombo").clearSelection();
      this.byId("typeRoomCombo").clearSelection();
      this.byId("bedroomsCombo").clearSelection();
      this.byId("bedsCombo").clearSelection();
      this.byId("bathroomsCombo").clearSelection();
      this.byId("priceMinCombo").setValue("");
      this.byId("priceMaxCombo").setValue("");

      this.getOwnerComponent().getModel("filteredBy").setData({});
    },

  });
});
