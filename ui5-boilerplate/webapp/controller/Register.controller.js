sap.ui.define(
  [
    'ui5boilerplate/controller/Base.controller',
    'sap/ui/model/json/JSONModel',
    'sap/m/MessageToast',
    'sap/m/MessageBox',
    'ui5boilerplate/utils/Validations',
    'jquery.sap.global',
  ],
  (BaseController, JsonModel, MessageToast, MessageBox, Validations, jQuery) => {
    return BaseController.extend('ui5boilerplate.controller.Register', {
      onInit() {
        let newUser = new JsonModel({
          countryRegion: 'Afghanistan - AF',
        });
        this.getView().setModel(newUser, 'newUser');

        let cData = {
          SelectedCountry: 'Country',
          CountryCollection: [
            { StatusId: 'Afghanistan - AF', Status: 'Afghanistan - AF' },
            { StatusId: 'Aland Islands - AX', Status: 'Aland Islands - AX' },
            { StatusId: 'Albania - AL', Status: 'Albania - AL' },
            { StatusId: 'American Samoa - AS', Status: 'American Samoa - AS' },
            { StatusId: 'Andorra - AD', Status: 'Andorra - AD' },
            { StatusId: 'Angola - AO', Status: 'Angola - AO' },
            { StatusId: 'Anguilla - AI', Status: 'Anguilla - AI' },
            { StatusId: 'Antigua & Barbuda - AG', Status: 'Antigua & Barbuda - AG' },
            { StatusId: 'Argentina - AR', Status: 'Argentina - AR' },
            { StatusId: 'Armenia - AM', Status: 'Armenia - AM' },
            { StatusId: 'Aruba - AW', Status: 'Aruba - AW' },
            { StatusId: 'Australia - AU', Status: 'Australia - AU' },
            { StatusId: 'Austria - AT', Status: 'Austria - AT' },
            { StatusId: 'Azerbaijan - AZ', Status: 'Azerbaijan - AZ' },
            { StatusId: 'Bahamas - BS', Status: 'Bahamas - BS' },
            { StatusId: 'Bahrain - BH', Status: 'Bahrain - BH' },
            { StatusId: 'Bangladesh - BD', Status: 'Bangladesh - BD' },
            { StatusId: 'Barbados - BB', Status: 'Barbados - BB' },
            { StatusId: 'Belarus - BY', Status: 'Belarus - BY' },
            { StatusId: 'Belgium - BE', Status: 'Belgium - BE' },
            { StatusId: 'Belize - BZ', Status: 'Belize - BZ' },
            { StatusId: 'Benin - BJ', Status: 'Benin - BJ' },
            { StatusId: 'Bermuda - BM', Status: 'Bermuda - BM' },
            { StatusId: 'Bhutan - BT', Status: 'Bhutan - BT' },
            { StatusId: 'Bolivia - BO', Status: 'Bolivia - BO' },
            { StatusId: 'Bosnia & Herzegovina - BA', Status: 'Bosnia & Herzegovina - BA' },
            { StatusId: 'Botswana - BW', Status: 'Botswana - BW' },
            { StatusId: 'Brazil - BR', Status: 'Brazil - BR' },
            { StatusId: 'British Indian Ocean Territory - IO', Status: 'British Indian Ocean Territory - IO' },
            { StatusId: 'British Virgin Islands - VG', Status: 'British Virgin Islands - VG' },
            { StatusId: 'Brunei - BN', Status: 'Brunei - BN' },
            { StatusId: 'Bulgaria - BG', Status: 'Bulgaria - BG' },
            { StatusId: 'Burkina Faso - BF', Status: 'Burkina Faso - BF' },
            { StatusId: 'Burundi - BI', Status: 'Burundi - BI' },
            { StatusId: 'Cambodia - KH', Status: 'Cambodia - KH' },
            { StatusId: 'Cameroon - CM', Status: 'Cameroon - CM' },
            { StatusId: 'Canada - CA', Status: 'Canada - CA' },
            { StatusId: 'Cape Verde - CV', Status: 'Cape Verde - CV' },
            { StatusId: 'Bonaire, Sint Eustatius and Saba - BQ', Status: 'Bonaire, Sint Eustatius and Saba - BQ' },
            { StatusId: 'Cayman Islands - KY', Status: 'Cayman Islands - KY' },
            { StatusId: 'Central African Republic - CF', Status: 'Central African Republic - CF' },
            { StatusId: 'Chad - TD', Status: 'Chad - TD' },
            { StatusId: 'China - CN', Status: 'China - CN' },
            { StatusId: 'Christmas Island - CX', Status: 'Christmas Island - CX' },
            { StatusId: 'Cocos (Keeling) Islands - CC', Status: 'Cocos (Keeling) Islands - CC' },
            { StatusId: 'Colombia - CO', Status: 'Colombia - CO' },
            { StatusId: 'Comoros - KM', Status: 'Comoros - KM' },
            { StatusId: 'Congo - CG', Status: 'Congo - CG' },
            { StatusId: 'Cook Islands - CK', Status: 'Cook Islands - CK' },
            { StatusId: 'Costa Rica - CR', Status: 'Costa Rica - CR' },
            { StatusId: 'Croatia - HR', Status: 'Croatia - HR' },
            { StatusId: 'Cuba - CU', Status: 'Cuba - CU' },
            { StatusId: 'Curacao - CW', Status: 'Curacao - CW' },
            { StatusId: 'Cyprus - CY', Status: 'Cyprus - CY' },
            { StatusId: 'Czechia - CZ', Status: 'Czechia - CZ' },
            { StatusId: 'Democratic Republic of the Congo - CD', Status: 'Democratic Republic of the Congo - CD' },
            { StatusId: 'Denmark - DK', Status: 'Denmark - DK' },
            { StatusId: 'Djibouti - DJ', Status: 'Djibouti - DJ' },
            { StatusId: 'Dominica - DM', Status: 'Dominica - DM' },
            { StatusId: 'Dominican Republic - DO', Status: 'Dominican Republic - DO' },
            { StatusId: 'Timor-Leste - TL', Status: 'Timor-Leste - TL' },
            { StatusId: 'Ecuador - EC', Status: 'Ecuador - EC' },
            { StatusId: 'Egypt - EG', Status: 'Egypt - EG' },
            { StatusId: 'El Salvador - SV', Status: 'El Salvador - SV' },
            { StatusId: 'Equatorial Guinea - GQ', Status: 'Equatorial Guinea - GQ' },
            { StatusId: 'Eritrea - EE', Status: 'Eritrea - EE' },
            { StatusId: 'Ethiopia - ET', Status: 'Ethiopia - ET' },
            { StatusId: 'Falkland Islands(Islas Malvinas) - FK', Status: 'Falkland Islands(Islas Malvinas) - FK' },
            { StatusId: 'Feroe Islands - FO', Status: 'Feroe Islands - FO' },
            { StatusId: 'Fiji - FJ', Status: 'Fiji - FJ' },
            { StatusId: 'Finland - FI', Status: 'Finland- FI' },
            { StatusId: 'France - FR', Status: 'France - FR' },
            { StatusId: 'French Polynesia - PF', Status: 'French Polynesia - PF' },
            { StatusId: 'Gabon - GA', Status: 'Gabon - GA' },
            { StatusId: 'Gambia - GM', Status: 'Gambia - GM' },
            { StatusId: 'Georgia - GE', Status: 'Georgia - GE' },
            { StatusId: 'Germany - DE', Status: 'Germany - DE' },
            { StatusId: 'Ghana - GH', Status: 'Ghana - GH' },
            { StatusId: 'Gibraltar - GI', Status: 'Gibraltar - GI' },
            { StatusId: 'Greece - GR', Status: 'Greece - GR' },
            { StatusId: 'Greenland - GL', Status: 'Greenland - GL' },
            { StatusId: 'Grenada - GD', Status: 'Grenada - GD' },
            { StatusId: 'Guadeloupe - GP', Status: 'Guadeloupe - GP' },
            { StatusId: 'Guam - GU', Status: 'Guam - GU' },
            { StatusId: 'Guatemala - GT', Status: 'Guatemala - GT' },
            { StatusId: 'Guernsey - GG', Status: 'Guernsey - GG' },
            { StatusId: 'Guinea - GN', Status: 'Guinea - GN' },
            { StatusId: 'Guinea-Bissau - GW', Status: 'Guinea-Bissau - GW' },
            { StatusId: 'Guyana - GY', Status: 'Guyana - GY' },
            { StatusId: 'Haiti - HT', Status: 'Haiti - HT' },
            { StatusId: 'Honduras - HN', Status: 'Honduras - HN' },
            { StatusId: 'Hong Kong - HK', Status: 'Hong Kong - HK' },
            { StatusId: 'Hungary - HU', Status: 'Hungary - HU' },
            { StatusId: 'Iceland - IS', Status: 'Iceland - IS' },
            { StatusId: 'India - IN', Status: 'India - IN' },
            { StatusId: 'Indonesia - ID', Status: 'Indonesia - ID' },
            { StatusId: 'Iraq - IQ', Status: 'Iraq - IQ' },
            { StatusId: 'Ireland - IE', Status: 'Ireland - IE' },
            { StatusId: 'Isle of Man - IM', Status: 'Isle of Man - IM' },
            { StatusId: 'Israel - IL', Status: 'Israel - IL' },
            { StatusId: 'Italy - IT', Status: 'Italy - IT' },
            { StatusId: 'Cote de Ivoire - CI', Status: 'Cote de Ivoire - CI' },
            { StatusId: 'Jamaica - KM', Status: 'Jamaica - KM' },
            { StatusId: 'Japan - JP', Status: 'Japan - JP' },
            { StatusId: 'Jersey - JE', Status: 'Jersey - JE' },
            { StatusId: 'Jordan - JO', Status: 'Jordan - JO' },
            { StatusId: 'Kazakhstan - KZ', Status: 'Kazakhstan - KZ' },
            { StatusId: 'Kenya - KE', Status: 'Kenya - KE' },
            { StatusId: 'Kiribati - KI', Status: 'Kiribati - KI' },
            { StatusId: 'Kosovo - XK', Status: 'Kosovo - XK' },
            { StatusId: 'Kuwait - KW', Status: 'Kuwait - KW' },
            { StatusId: 'Kyrgyzstan - KG', Status: 'Kyrgyzstan - KG' },
            { StatusId: 'Laos - LA', Status: 'Laos - LA' },
            { StatusId: 'Latvia - LV', Status: 'Latvia - LV' },
            { StatusId: 'Lebanon - LB', Status: 'Lebanon - LB' },
            { StatusId: 'Lesotho - LS', Status: 'Lesotho - LS' },
            { StatusId: 'Liberia - LR', Status: 'Liberia - LR' },
            { StatusId: 'Libya - LY', Status: 'Libya - LY' },
            { StatusId: 'Liechtenstein - LI', Status: 'Liechtenstein - LI' },
            { StatusId: 'Lithuania - LT', Status: 'Lithuania - LT' },
            { StatusId: 'Luxembourg - LU', Status: 'Luxembourg - LU' },
            { StatusId: 'Macau - MO', Status: 'Macau - MO' },
            { StatusId: 'North Macedonia - MK', Status: 'North Macedonia - MK' },
            { StatusId: 'Madagascar - MG', Status: 'Madagascar - MG' },
            { StatusId: 'Malawi - MW', Status: 'Malawi - MW' },
            { StatusId: 'Malaysia - MY', Status: 'Malaysia - MY' },
            { StatusId: 'Maldives - MV', Status: 'Maldives - MV' },
            { StatusId: 'Mali - ML', Status: 'Mali - ML' },
            { StatusId: 'Malta - MT', Status: 'Malta - MT' },
            { StatusId: 'Marshall Islands - MH', Status: 'Marshall Islands - MH' },
            { StatusId: 'Martinique - MQ', Status: 'Martinique - MQ' },
            { StatusId: 'Mauritania - MR', Status: 'Mauritania - MR' },
            { StatusId: 'Mauritius - MU', Status: 'Mauritius - MU' },
            { StatusId: 'Mayotte - YT', Status: 'Mayotte - YT' },
            { StatusId: 'Mexico - MX', Status: 'Mexico - MX' },
            { StatusId: 'Micronesia - FM', Status: 'Micronesia - FM' },
            { StatusId: 'Moldova - MD', Status: 'Moldova - MD' },
            { StatusId: 'Monaco - MC', Status: 'Monaco - MC' },
            { StatusId: 'Mongolia - MN', Status: 'Mongolia - MN' },
            { StatusId: 'Montenegro - ME', Status: 'Montenegro - ME' },
            { StatusId: 'Montserrat - MS', Status: 'Montserrat - MS' },
            { StatusId: 'Morocco - MA', Status: 'Morocco - MA' },
            { StatusId: 'Mozambique - MZ', Status: 'Mozambique - MZ' },
            { StatusId: 'Myanmar - MM', Status: 'Myanmar - MM' },
            { StatusId: 'Namibia - NA', Status: 'Namibia - NA' },
            { StatusId: 'Nauru - NR', Status: 'Nauru - NR' },
            { StatusId: 'Nepal - NP', Status: 'Nepal - NP' },
            { StatusId: 'Netherlands - NL', Status: 'Netherlands - NL' },
            { StatusId: 'New Caledonia - NC', Status: 'New Caledonia - NC' },
            { StatusId: 'New Zeakand - NZ', Status: 'New Zeakand - NZ' },
            { StatusId: 'Nicaragua - NI', Status: 'Nicaragua - NI' },
            { StatusId: 'Niger - NE', Status: 'Niger - NE' },
            { StatusId: 'Nigeria - NG', Status: 'Nigeria - NG' },
            { StatusId: 'Niue - NU', Status: 'Niue - NU' },
            { StatusId: 'Norfolk Island - NF', Status: 'Norfolk Island - NF' },
            { StatusId: 'Northern Mariana Islands - MP', Status: 'Northern Mariana Islands - MP' },
            { StatusId: 'Norway - NO', Status: 'Norway - NO' },
            { StatusId: 'Oman - OM', Status: 'Oman - OM' },
            { StatusId: 'Pakistan - PK', Status: 'Pakistan - PK' },
            { StatusId: 'Palau - PW', Status: 'Palau - PW' },
            { StatusId: 'Palestinian Territories - PS', Status: 'Palestinian Territories - PS' },
            { StatusId: 'Panama - PA', Status: 'Panama - PA' },
            { StatusId: 'Papua New Guinea - PG', Status: 'Papua New Guinea - PG' },
            { StatusId: 'Paraguay - PY', Status: 'Paraguay - PY' },
            { StatusId: 'Peru - PE', Status: 'Peru - PE' },
            { StatusId: 'Philippines - PH', Status: 'Philippines - PH' },
            { StatusId: 'Pitcairn Islands - PN', Status: 'Pitcairn Islands - PN' },
            { StatusId: 'Poland - PL', Status: 'Poland - PL' },
            { StatusId: 'Portugal - PT', Status: 'Portugal - PT' },
            { StatusId: 'Puerto Rico - PR', Status: 'Puerto Rico - PR' },
            { StatusId: 'Qatar - QA', Status: 'Qatar - QA' },
            { StatusId: 'Reunion - RE', Status: 'Reunion - RE' },
            { StatusId: 'Romania - RO', Status: 'Romania - RO' },
            { StatusId: 'Russia - RU', Status: 'Russia - RU' },
            { StatusId: 'Rwanda - RW', Status: 'Rwanda - RW' },
            { StatusId: 'St. Barthelemy - BL', Status: 'St. Barthelemy - BL' },
            { StatusId: 'St. Helena - SH', Status: 'St. Helena - SH' },
            { StatusId: 'St. Kitts & Nevis - KN', Status: 'St. Kitts & Nevis - KN' },
            { StatusId: 'St. Lucia - LC', Status: 'St. Lucia - LC' },
            { StatusId: 'St. Martin - MF', Status: 'St. Martin - MF' },
            { StatusId: 'St. Pierre & Miquelon - PM', Status: 'St. Pierre & Miquelon - PM' },
            { StatusId: 'St. Vincent & Grenadines - VC', Status: 'St. Vincent & Grenadines - VC' },
            { StatusId: 'Samoa - WS', Status: 'Samoa - WS' },
            { StatusId: 'San Marino - SM', Status: 'San Marino - SM' },
            { StatusId: 'Sao Tome & Principe - ST', Status: 'Sao Tome & Principe - ST' },
            { StatusId: 'Saudi Arabia - SA', Status: 'Saudi Arabia - SA' },
            { StatusId: 'Senegal - SN', Status: 'Senegal - SN' },
            { StatusId: 'Serbia - RS', Status: 'Serbia - RS' },
            { StatusId: 'Seychelles - SC', Status: 'Seychelles - SC' },
            { StatusId: 'Sierra Leone - SL', Status: 'Sierra Leone - SL' },
            { StatusId: 'Singapore - SG', Status: 'Singapore - SG' },
            { StatusId: 'Sint Maarten - SX', Status: 'Sint Maarten - SX' },
            { StatusId: 'Slovakia - SK', Status: 'Slovakia - SK' },
            { StatusId: 'Slovenia - SI', Status: 'Slovenia - SI' },
            { StatusId: 'Solomon Islands - SB', Status: 'Solomon Islands - SB' },
            { StatusId: 'Somalia - SO', Status: 'Somalia - SO' },
            { StatusId: 'South Africa - ZA', Status: 'South Africa - ZA' },
            { StatusId: 'South Georgia & South Sandwich Islands - GS', Status: 'South Georgia & South Sandwich Islands - GS' },
            { StatusId: 'South Korea - KR', Status: 'South Korea - KR' },
            { StatusId: 'South Sudan - SS', Status: 'South Sudan - SS' },
            { StatusId: 'Spain - ES', Status: 'Spain - ES' },
            { StatusId: 'Sri Lanka - LK', Status: 'Sri Lanka - LK' },
            { StatusId: 'Sudan - SD', Status: 'Sudan - SD' },
            { StatusId: 'Suriname - SR', Status: 'Suriname - SR' },
            { StatusId: 'Svalbard & Jan Mayen - SJ', Status: 'Svalbard & Jan Mayen - SJ' },
            { StatusId: 'Eswatini - SZ', Status: 'Eswatini - SZ' },
            { StatusId: 'Sweden - SE', Status: 'Sweden - SE' },
            { StatusId: 'Switzerland - CH', Status: 'Switzerland - CH' },
            { StatusId: 'Taiwan - TW', Status: 'Taiwan - TW' },
            { StatusId: 'Tajikistan - TJ', Status: 'Tajikistan - TJ' },
            { StatusId: 'Tanzania - TZ', Status: 'Tanzania - TZ' },
            { StatusId: 'Thailand - TH', Status: 'Thailand - TH' },
            { StatusId: 'Togo - TG', Status: 'Togo - TG' },
            { StatusId: 'Tokelau - TK', Status: 'Tokelau - TK' },
            { StatusId: 'Tonga - TO', Status: 'Tonga - TO' },
            { StatusId: 'Trinidad & Tobago - TT', Status: 'Trinidad & Tobago - TT' },
            { StatusId: 'Tunisia - TN', Status: 'Tunisia - TN' },
            { StatusId: 'Turkey - TR', Status: 'Turkey - TR' },
            { StatusId: 'Turkmenistan - TM', Status: 'Turkmenistan - TM' },
            { StatusId: 'Turks & Caicos Islands - TC', Status: 'Turks & Caicos Islands - TC' },
            { StatusId: 'Tuvalu - TV', Status: 'Tuvalu - TV' },
            { StatusId: 'U.S Virgin Islands - VI', Status: 'U.S Virgin Islands - VI' },
            { StatusId: 'Uganda - UG', Status: 'Uganda - UG' },
            { StatusId: 'Ukraine - UA', Status: 'Ukraine - UA' },
            { StatusId: 'United Arab Emirates - AE', Status: 'United Arab Emirates - AE' },
            { StatusId: 'United Kingdom - GB', Status: 'United Kingdom - GB' },
            { StatusId: 'United States - US', Status: 'United States - US' },
            { StatusId: 'Uruguay - UY', Status: 'Uruguay - UY' },
            { StatusId: 'Uzbekistan - UZ', Status: 'Uzbekistan - UZ' },
            { StatusId: 'Vanuatu - VU', Status: 'Vanuatu - VU' },
            { StatusId: 'Vatican City - VA', Status: 'Vatican City - VA' },
            { StatusId: 'Venezuela - VE', Status: 'Venezuela - VE' },
            { StatusId: 'Vietnam - VN', Status: 'Vietnam - VN' },
            { StatusId: 'Wallis & Futuna - WF', Status: 'Wallis & Futuna - WF' },
            { StatusId: 'Western Sahara - EH', Status: 'Western Sahara - EH' },
            { StatusId: 'Yemen - YE', Status: 'Yemen - YE' },
            { StatusId: 'Zambia - ZM', Status: 'Zambia - ZM' },
            { StatusId: 'Zimbabwe - ZW', Status: 'Zimbabwe - ZM' },
          ],
          Editable: true,
          Enabled: true,
        };
        let cModel = new JsonModel(cData);
        this.getView().setModel(cModel, 'SelectedModel3');

        let dData = {
          SelectedCountry: 'Status',
          CountryCollection: [
            { StatusId: 'Proprietar', Status: 'Proprietar' },
            { StatusId: 'Vizitator/Chirias', Status: 'Vizitator/Chirias' }
          ],
          Editable: true,
          Enabled: true,
        };
        let dModel = new JsonModel(dData);
        this.getView().setModel(dModel, 'SelectedModel4');
        
      },

      onAfterRendering: function () {
        this.onChangePage();
      },

      onChangePage: function () {
        var oCarousel = this.byId('registerCarousel');
        setTimeout(function () {
          oCarousel.next();
        }, 4500);
      },

      onPressLoginHere: function (evt) {
        let newUser = new JsonModel();
        this.getView().setModel(newUser, 'newUser');
        let oRouter = this.getOwnerComponent().getRouter();
        oRouter.navTo('Login');
      },

      checkFirstName: function () {
        let userCheck = this.getView().getModel('newUser').getData();
        let firstName = userCheck.firstName;
        if (Validations.validateName(firstName)) {
          this.getView().getModel('newUser').setProperty('/firstNameState', 'None');
        } else {
          this.getView().getModel('newUser').setProperty('/firstNameState', 'Error');
        }
      },

      checkLastName: function () {
        let userCheck = this.getView().getModel('newUser').getData();
        let lastName = userCheck.lastName;
        if (Validations.validateName(lastName)) {
          this.getView().getModel('newUser').setProperty('/lastNameState', 'None');
        } else {
          this.getView().getModel('newUser').setProperty('/lastNameState', 'Error');
        }
      },

      checkPhoneNumber: function () {
        let userCheck = this.getView().getModel('newUser').getData();
        let phoneNumber = userCheck.phoneNumber;
        if (Validations.validatePhone(phoneNumber)) {
          this.getView().getModel('newUser').setProperty('/phoneNumberState', 'None');
        } else {
          this.getView().getModel('newUser').setProperty('/phoneNumberState', 'Error');
        }
      },

      checkEmail: function () {
        let userCheck = this.getView().getModel('newUser').getData();
        let email = userCheck.email;
        if (Validations.validateEmail(email)) {
          this.getView().getModel('newUser').setProperty('/emailState', 'None');
        } else {
          this.getView().getModel('newUser').setProperty('/emailState', 'Error');
        }
      },

      checkCity: function () {
        let userCheck = this.getView().getModel('newUser').getData();
        let city = userCheck.city;
        if (Validations.validateNoSpace(city)) {
          this.getView().getModel('newUser').setProperty('/cityState', 'None');
        } else {
          this.getView().getModel('newUser').setProperty('/cityState', 'Error');
        }
      },

      checkPassword: function () {
        let userCheck = this.getView().getModel('newUser').getData();
        let password = userCheck.password;
        if (Validations.validatePassword(password)) {
          this.getView().getModel('newUser').setProperty('/passwordState', 'None');
        } else {
          this.getView().getModel('newUser').setProperty('/passwordState', 'Error');
        }
      },

      checkConfirmPassword: function () {
        let userCheck = this.getView().getModel('newUser').getData();
        let confirmPassword = userCheck.confirm_password;
        if (confirmPassword) {
          this.getView().getModel('newUser').setProperty('/confirmPasswordState', 'None');
        } else {
          this.getView().getModel('newUser').setProperty('/confirmPasswordState', 'Error');
        }
      },

      onRegister: function () {
        const oData = this.getView().getModel('newUser').oData;
        const firstName = oData.firstName;
        const lastName = oData.lastName;
        const phoneNumber = oData.phoneNumber;
        const email = oData.email;
        const password = oData.password;
        const city = oData.city;
        const confirm_password = oData.confirm_password;

        console.log(oData);

        let oRouter = this.getOwnerComponent().getRouter();

        if (
          Validations.validateName(firstName) &&
          Validations.validateName(lastName) &&
          Validations.validatePhone(phoneNumber) &&
          Validations.validateEmail(email) &&
          Validations.validatePassword(password) &&
          city &&
          confirm_password
        ) {
          const data = JSON.parse(JSON.stringify(oData));
          data.password = this.encryptPassword(data.password);
          let newUser = new JsonModel({
            county: 'Alba',
          });
          this.post('http://localhost:3000/register', data)
            .then((response) => {
              MessageToast.show(this.getI18nMessage('WELCOME'));
              let newUser = new JsonModel();
              this.getView().setModel(newUser, 'newUser');
              oRouter.navTo('Login');
            })
            .catch((err) => {
              console.log(err);
              if (err.status === 401 || err.status === 400 || err.status === 409) {
                sap.m.MessageBox.error(`${err.responseJSON.error}` + '.');
              }
            });
          this.getView().getModel('newUser').setProperty('/firstNameState', 'None');
          this.getView().getModel('newUser').setProperty('/lastNameState', 'None');
          this.getView().getModel('newUser').setProperty('/phoneNumberState', 'None');
          this.getView().getModel('newUser').setProperty('/emailState', 'None');
          this.getView().getModel('newUser').setProperty('/passwordState', 'None');
          this.getView().getModel('newUser').setProperty('/cityState', 'None');
          this.getView().getModel('newUser').setProperty('/confirmPasswordState', 'None');
        } else {
          sap.m.MessageBox.error(this.getI18nMessage('HIGHLIGHTED_FIELDS'));
          if (!firstName || !Validations.validateName(firstName)) {
            this.getView().getModel('newUser').setProperty('/firstNameState', 'Error');
          }
          if (!lastName || !Validations.validateName(lastName)) {
            this.getView().getModel('newUser').setProperty('/lastNameState', 'Error');
          }
          if (!Validations.validatePhone(phoneNumber)) {
            this.getView().getModel('newUser').setProperty('/phoneNumberState', 'Error');
          }
          if (!Validations.validateEmail(email)) {
            this.getView().getModel('newUser').setProperty('/emailState', 'Error');
          }
          if (!password) {
            this.getView().getModel('newUser').setProperty('/passwordState', 'Error');
          }
          if (!city) {
            this.getView().getModel('newUser').setProperty('/cityState', 'Error');
          }
          if (!confirm_password) {
            this.getView().getModel('newUser').setProperty('/confirmPasswordState', 'Error');
          }
        }
      },
    });
  }
);
