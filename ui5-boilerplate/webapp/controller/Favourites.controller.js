sap.ui.define(
  [
    'ui5boilerplate/controller/Base.controller',
    'sap/ui/model/json/JSONModel',
    'sap/m/MessageToast',
    'ui5boilerplate/utils/Validations',
    'sap/m/MessageBox'
  ],
  (BaseController, JSONModel, MessageToast, Validations, MessageBox) => {
    return BaseController.extend('ui5boilerplate.controller.Favourites', {
      onInit: function () {
        const oRouter = this.getOwnerComponent().getRouter();
        oRouter.getRoute("Favourites").attachMatched(this.onRouteMatched, this);

      },

      onRouteMatched: async function () {
        this.verifyToken();

        let stateModel = new JSONModel({
          firstNameState: "None",
          lastNameState: "None",
          cityState: "None",
          phoneNumberState: "None",
          countyState: "None"
        });
        this.getView().setModel(stateModel, "stateModel");

        let deleteAccount = new JSONModel({
          chechBox1: false,
          checkBox2: false,
          checkBox3: false,
          suggestion:""
        });
        this.getView().setModel(deleteAccount, "deleteAccount");

        this.showBusyIndicator(2000, 0);
        let resArray = [];
        const data = await this.get("http://localhost:3000/userr/favourites")
          .then((data) => {
            this.byId("favGridList").setVisible(true);
            this.byId("favouritesText").setVisible(true);
            if (data.length > 0) {
              resArray = [...data];
              this.getOwnerComponent()
                .getModel("favouriteRooms")
                .setData(resArray);
            } else {
              this.getOwnerComponent().getModel("favouriteRooms").setData({});
            }
          })
          .catch((err) => {
            console.log(err);
          });

      },

      onPressSeeMoreFav: function (oEvent) {
        let selectedRoom = oEvent
          .getSource()
          .getBindingContext("favouriteRooms")
          .getObject();
        this.getView().getModel("selectedRoom").setData(selectedRoom);

        this.get(
          "http://localhost:3000/room/" +
          this.getView().getModel("selectedRoom").getData()._id
        )

          .then((response) => {
            let selectedRoomId = this.getView()
              .getModel("selectedRoom")
              .getData()._id;

            this.getRouter().navTo("RoomPage", { selectedId: selectedRoomId });
          })

          .catch((err) => {
            console.log(err);
          });
      },
    });
  }
);