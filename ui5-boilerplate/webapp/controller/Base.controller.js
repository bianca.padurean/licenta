sap.ui.define(
  [
    'sap/ui/core/mvc/Controller',
    'sap/ui/core/Fragment',
    'jquery.sap.global',
    'sap/ui/model/json/JSONModel',
    'sap/ui/core/BusyIndicator',
    'ui5boilerplate/utils/AjaxClient',
    'ui5boilerplate/utils/Credentials',
    'sap/m/MessageToast',
    'sap/m/MessageBox',
    'ui5boilerplate/utils/Validations'
  ],
  (Controller, Fragment, jQuery, JSONModel, BusyIndicator, AjaxClient, Credentials, MessageToast, MessageBox, Validations) => {
    return Controller.extend('ui5boilerplate.controller.BaseController', {
      AjaxClient: AjaxClient,
      onInit() {

      },
      onAfterRendering: function () {
        this.setupModels();
      },

      setupModels: function () {
        let amenitiesModel2 = new JSONModel();
        this.getView().setModel(amenitiesModel2, "amenitiesModel2");
      },

      getRouter: function () {
        return sap.ui.core.UIComponent.getRouterFor(this);
      },

      get: function (url) {
        return this.AjaxClient.get(url);
      },

      post: function (url, data) {
        return this.AjaxClient.post(url, data);
      },

      put: function (url, data) {
        return this.AjaxClient.put(url, data);
      },

      delete: function (url) {
        return this.AjaxClient.delete(url);
      },

      onPressMyProfile: function (oEvent) {
        const id = this.getUserId();

        this.get('http://localhost:3000/user/' + id)
          .then((data) => {
            let userModel = new JSONModel(data);
            this.getView().setModel(userModel, 'userModel');
          })
          .catch((err) => {
            console.log(err);
          });

        if (!this.ppDialog) {
          this.ppDialog = Fragment.load({
            id: this.getView().getId(),
            name: 'ui5boilerplate.view.ProfileDialog',
            controller: this,
          }).then((ooDialog) => {
            this.getView().addDependent(ooDialog);
            return ooDialog;
          });
        }
        this.ppDialog.then(function (ooDialog) {
          ooDialog.open();
        });

        
      },

      handleBack: function () {
        this.byId('ProfileDialog').close();
      },

      onPressTop5: function (evt) {
        let oRouter = this.getOwnerComponent().getRouter();
        oRouter.navTo('Top3');
      },

      onPressHome: function (evt) {
        let oRouter = this.getOwnerComponent().getRouter();
        oRouter.navTo('home');
      },

      onPressCreateAd: function (evt) {
        let oRouter = this.getOwnerComponent().getRouter();
        oRouter.navTo('CreateAd');
      },

      onPressManageListings: function (evt) {
        let oRouter = this.getOwnerComponent().getRouter();
        oRouter.navTo('Listings');
      },

      onPressRegister: function (evt) {
        let oRouter = this.getOwnerComponent().getRouter();
        oRouter.navTo('Register');
      },

      onPressFavourites: function (evt) {
        let oRouter = this.getOwnerComponent().getRouter();
        oRouter.navTo('Favourites');
      },

      onPressAboutUs: function (evt) {
        let oRouter = this.getOwnerComponent().getRouter();
        oRouter.navTo('AboutUs');
      },

      encryptPassword: function (password) {
        const secret = Credentials.getEncryptionSecret();
        console.log(password);
        console.log(secret);
        const encryptedPassword = CryptoJS.AES.encrypt(password, secret).toString();
        console.log(encryptedPassword);
        return encryptedPassword;
      },

      verifyToken: async function () {
        const oRouter = this.getOwnerComponent().getRouter();
        if (!document.cookie) {
          oRouter.navTo('Login');
          return;
        }

        this.post('http://localhost:3000/auth').catch((err) => {
          console.log('Error' + err);
          oRouter.navTo('Login');
        });

        const id = this.getUserId();
        this.get('http://localhost:3000/user/' + id)
        .then((data) => {
          let userModel = new JSONModel(data);
          this.getView().setModel(userModel, 'userModel');
          if(this.getView().getModel("userModel").getData().status == "Vizitator/Chirias")
       {
        this.byId("createAdButton").setVisible(false);
        this.byId("manageListingsButton").setVisible(false);
        
       }
       else{
        this.byId("createAdButton").setVisible(true);
        this.byId("manageListingsButton").setVisible(true);
       }
        })
        .catch((err) => {
          console.log(err);
        });
      },

      getToken: function () {
        const cookieData = document.cookie.split(/[\ ;=]/);

        for (let i = 0; i < cookieData.length; i++) {
          if (cookieData[i] === 'token') return cookieData[i + 1];
        }
      },

      getUserId: function () {
        const cookieData = document.cookie.split(/[\ ;=]/);

        for (let i = 0; i < cookieData.length; i++) {
          if (cookieData[i] === 'id') return cookieData[i + 1];
        }
      },

      getI18nMessage: function (sI18n, arg) {
        var oBundle = this.getView().getModel('i18n').getResourceBundle();
        var sMsg = oBundle.getText(sI18n, arg);
        return sMsg;
      },

      handleCancelLogout: function () {
        this.byId("logoutDialog").close();
      },

      hideBusyIndicator: function () {
        BusyIndicator.hide();
      },


      showBusyIndicator: function (iDuration, iDelay) {
        BusyIndicator.show(iDelay);

        if (iDuration && iDuration > 0) {
          if (this._sTimeoutId) {
            clearTimeout(this._sTimeoutId);
            this._sTimeoutId = null;
          }

          this._sTimeoutId = setTimeout(
            function () {
              this.hideBusyIndicator();
            }.bind(this),
            iDuration
          );
        }
      },

      handleYesLogout: function () {
        var oRouter = this.getOwnerComponent().getRouter();
        document.cookie.split(";").forEach(function (c) { document.cookie = c.replace(/^ +/, "").replace(/=.*/, "=;expires=" + new Date().toUTCString() + ";path=/"); });
        this.showBusyIndicator(1000, 0);
        oRouter.navTo("Login");
      },


      handleLogoutPress: function () {

        if (!this.ssDialog) {
          this.ssDialog = Fragment.load({
            id: this.getView().getId(),
            name: 'ui5boilerplate.view.Logout',
            controller: this,
          }).then((ttDialog) => {
            this.getView().addDependent(ttDialog);
            return ttDialog;
          });
        }
        this.ssDialog.then(function (ttDialog) {
          ttDialog.open();
        });
      },

      onEditProfileDialogPress: function () {
        if (!this.ddDialog) {
          this.ddDialog = Fragment.load({
            id: this.getView().getId(),
            name: 'ui5boilerplate.view.EditProfileDialog',
            controller: this,
          }).then((ccDialog) => {
            this.getView().addDependent(ccDialog);
            return ccDialog;
          });
        }
        this.ddDialog.then(function (ccDialog) {
          ccDialog.open();
        });
      },

      handleBackEdit: function () {
        this.onPressMyProfile();
        this.getView().getModel("stateModel").setProperty("/lastNameState", "None");
        this.getView().getModel("stateModel").setProperty("/firstNameState", "None");
        this.getView().getModel("stateModel").setProperty("/phoneNumberState", "None");
        this.getView().getModel("stateModel").setProperty("/cityState", "None");
        this.byId("EditProfileDialog").close();

      },

      handleBackEditAd: function(){
        this.byId("EditAdDialog").close();
      },


      handleBackDelete: function () {
        let deleteAccount = new JSONModel();
        this.getView().setModel(deleteAccount, 'deleteAccount');
        this.byId('DeleteAccountDialog').close();
      },



      onPressDeleteAccountRequest: function () {
        if (!this.aaDialog) {
          this.aaDialog = Fragment.load({
            id: this.getView().getId(),
            name: 'ui5boilerplate.view.DeleteAccountDialog',
            controller: this,
          }).then((bbDialog) => {
            this.getView().addDependent(bbDialog);
            return bbDialog;
          });
        }
        this.aaDialog.then(function (bbDialog) {
          bbDialog.open();
        });
      },

      onPressDeleteAccountAccept: function () {

        if (this.byId("checkbox1").getSelected() == false &&
          this.byId("checkbox2").getSelected() == false &&
          this.byId("checkbox3").getSelected() == false) {
          MessageBox.error(this.getI18nMessage("MESSAGE_BOX_DELETE_REASON"));
        }
        else {
          if (!this.eeDialog) {
            this.eeDialog = Fragment.load({
              id: this.getView().getId(),
              name: 'ui5boilerplate.view.DeleteAccountAcceptDialog',
              controller: this,
            }).then((ffDialog) => {
              this.getView().addDependent(ffDialog);
              return ffDialog;
            });
          }
          this.eeDialog.then(function (ffDialog) {
            ffDialog.open();
          });
        }
      },

      handleCancelDelete: function () {
        this.byId("DeleteAccountAcceptDialog").close();
      },

      handleYesDeleteAccount: async function () {
        const cookieData = document.cookie.split(" ");
        const id = cookieData[1].split("=")[1];
        console.log(id);

        const data = await this.get("http://localhost:3000/user/" + id);
        console.log(data.listedAds);
        if (data.listedAds.length > 0) {
          data.listedAds.forEach(async (element) => {

            const response = this.delete(
              "http://localhost:3000/roomm/" + element
            );

          });
        } else {
          this.getOwnerComponent().getModel("listedRooms").setData({});
        }

        await this.delete("http://localhost:3000/user/deleteUser/" + id)
          .then((response) => {
            this.getView().getModel("deleteAccount").setProperty("/suggestion", "");
            this.getView().getModel("deleteAccount").setProperty("/checkBox1", false);
            this.getView().getModel("deleteAccount").setProperty("/checkBox2", false);
            this.getView().getModel("deleteAccount").setProperty("/checkBox3", false);
            this.handleYesLogout();
            
            console.log("deleted!")
          })
          .catch((err) => {
            console.log("err", err);
          });

      },

      onPressSave() {
        console.log(this.getView().getModel("userModel"));
        if (
          this.getView().getModel("stateModel").getData().lastNameState ===
          "Error" ||
          this.getView().getModel("stateModel").getData().firstNameState ===
          "Error" ||
          this.getView().getModel("stateModel").getData().cityState ===
          "Error" ||
          this.getView().getModel("stateModel").getData().phoneNumberState ===
          "Error"
        ) {
          MessageBox.error(this.getI18nMessage("MESSAGEBOXINCORRECTINPUT"));
        } else {
          let data = this.getView().getModel("userModel").getData();
          this.put(
            "http://localhost:3000/user/update/" +
            document.cookie.split(" ")[1].split("=")[1],
            data
          )
            .then((updateData) => {
              MessageToast.show(this.getI18nMessage("MESSAGEBOXUPDATED"));
              this.getView().getModel("userModel").setData(updateData);
              this.byId('EditProfileDialog').close();
              this.onPressMyProfile();
            })
            .catch((err) => {
              console.log(err);
            });
        }
      },

      onCheckLastName() {
        let userCheck = this.getView().getModel("userModel").getData();
        let lastName = userCheck.lastName;
        if (Validations.validateName(lastName)) {
          this.getView().getModel("stateModel").setProperty("/lastNameState", "None");
        } else {
          this.getView().getModel("stateModel").setProperty("/lastNameState", "Error");
        }
      },

      onCheckFirstName() {
        let userCheck = this.getView().getModel("userModel").getData();
        let firstName = userCheck.firstName;
        if (Validations.validateName(firstName)) {
          this.getView().getModel("stateModel").setProperty("/firstNameState", "None");
        } else {
          this.getView().getModel("stateModel").setProperty("/firstNameState", "Error");
        }
      },

      onCheckNumber() {
        let userCheck = this.getView().getModel("userModel").getData();
        let phoneNumber = userCheck.phoneNumber;
        console.log(phoneNumber);
        if (Validations.validatePhone(phoneNumber)) {
          this.getView().getModel("stateModel").setProperty("/phoneNumberState", "None");
        } else {
          this.getView().getModel("stateModel").setProperty("/phoneNumberState", "Error");
        }
      },

      onCheckCity() {
        let userCheck = this.getView().getModel("userModel").getData();
        let city = userCheck.city;
        if (Validations.validateName(city)) {
          this.getView().getModel("stateModel").setProperty("/cityState", "None");
        } else {
          this.getView().getModel("stateModel").setProperty("/cityState", "Error");
        }
      },

      onPressSeeMore: function (oEvent) {

        let selectedRoom = oEvent
          .getSource()
          .getBindingContext("roomAds")
          .getObject();
        this.getOwnerComponent().getModel("selectedRoom").setData(selectedRoom);
        console.log(selectedRoom);


        let amenitiesModel = new JSONModel();
        this.getView().setModel(amenitiesModel, "amenitiesModel");
        this.getView().getModel("amenitiesModel").setData(selectedRoom);

        let wifi = this.getView().getModel("amenitiesModel").getData().wifi;


        if (wifi === true) {
          this.getView().getModel("amenitiesModel2").setProperty("/wifi", "yes");
          this.getView().getModel("amenitiesModel2").refresh();
        }


        let wifi2 = this.getView().getModel("amenitiesModel2").getData().wifi;
        console.log(wifi2);

        this.get(
          "http://localhost:3000/room/" +
          this.getView().getModel("selectedRoom").getData()._id
        )
          .then((response) => {
            let selectedRoomId = this.getView()
              .getModel("selectedRoom")
              .getData()._id;
            this.getRouter().navTo("RoomPage", { selectedId: selectedRoomId });
          })
          .catch((err) => {
            console.log(err);
          });
      },

      onPressSubmitRate() {
        let roomID = this.getOwnerComponent()
          .getModel("selectedRoom")
          .getData()._id;
        console.log(roomID);
        let stars = this.byId("ratingIndicator").getValue();
        console.log(stars);


        this.post(`http://localhost:3000/rate`, "stars=" + stars + "&roomID=" + roomID)
          .then((data) => {
            let ratedRoom = new JSONModel(data);
            this.getView().setModel(ratedRoom, "ratedRoom");

            let commentsModel = new JSONModel(this.getView().getModel("ratedRoom").getData().comments);
            this.getView().setModel(commentsModel, "commentsModel");

            MessageToast.show(this.getI18nMessage("Feedback_UPLOAD"));
            this.showBusyIndicator(1000, 0);

            this.byId("ratingIndicator").setValue(0);
            let oRouter = this.getOwnerComponent().getRouter();
            oRouter.navTo('home');
          })
          .catch((err) => {
            console.log(err)
          })
      },

      addComment: function () {
        let selectedRoom = this.getOwnerComponent().getModel("selectedRoom").getData()._id;
        let comment = String(this.byId("inputComments").getValue());
        this.post(`http://localhost:3000/comment`, "selectedRoom=" + selectedRoom + "&comment=" + comment)
          .then((data) => {
            this.get("http://localhost:3000/room/" +
              this.getOwnerComponent().getModel("selectedRoom").getData()._id)
              .then((response) => {
                this.getOwnerComponent().getModel("selectedRoom").setData(response);
                this.getView().getModel("commentsModel").setData(this.getOwnerComponent().getModel("selectedRoom").getData().comments)
              })
              .catch((err) => {
                console.log(err);
              });
          }).catch((err) => {
            console.log(err)
          })
      },
    });
  }
);