sap.ui.define(
    [
      'ui5boilerplate/controller/Base.controller',
      'sap/ui/model/json/JSONModel',
      'ui5boilerplate/utils/AjaxClient',
    ],
    (BaseController, JSONModel, AjaxClient) => {
      return BaseController.extend('ui5boilerplate.controller.Map', {
        AjaxClient: AjaxClient,
        onInit: function () {
          const oRouter = this.getOwnerComponent().getRouter();
          oRouter.getRoute("Map").attachMatched(this.onRouteMatched, this);
        },
  
        onRouteMatched: function (oEvent) {
          this.verifyToken();
          this.showBusyIndicator(2000, 0);

          this.get(
            "http://localhost:3000/room/" +
            oEvent.getParameter("arguments").selectedId
          )
            .then((response) => {
              this.getView().getModel("selectedRoom").setData(response);
            })
            .catch((err) => {
              console.log(err);
            });
        },

        onPressLegend: function() {
			if (this.byId("vbi").getLegendVisible() == true) {
				this.byId("vbi").setLegendVisible(false);
				this.byId("btnLegend").setTooltip("Show legend");
			} else {
				this.byId("vbi").setLegendVisible(true);
				this.byId("btnLegend").setTooltip("Hide legend");
			}
		},

        onPressResize: function() {
			if (this.byId("btnResize").getTooltip() == "Minimize") {
				if (sap.ui.Device.system.phone) {
					this.byId("vbi").minimize(132, 56, 1320, 560);// Height: 3,5 rem; Width: 8,25 rem
				} else {
					this.byId("vbi").minimize(168, 72, 1680, 720);// Height: 4,5 rem; Width: 10,5 rem
				}
				this.byId("btnResize").setTooltip("Maximize");
			} else {
				this.byId("vbi").maximize();
				this.byId("btnResize").setTooltip("Minimize");
			}
		},
        
      });
    }
  );
  