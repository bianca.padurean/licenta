sap.ui.define(
    [
        'ui5boilerplate/controller/Base.controller',
        'sap/ui/model/json/JSONModel',
        'sap/m/MessageToast',
        'ui5boilerplate/utils/Validations',
        'sap/m/MessageBox',
        'sap/ui/core/Fragment'
    ],
    (BaseController, JSONModel, MessageToast, Validations, MessageBox, Fragment) => {
        return BaseController.extend('ui5boilerplate.controller.Listings', {
            onInit: function () {
                const oRouter = this.getOwnerComponent().getRouter();
                oRouter
                    .getRoute("Listings")
                    .attachMatched(this.historyPageFunction, this);
            },

            historyPageFunction: function () {
                this.verifyToken();
                this.getListings();
                this.showBusyIndicator(2000, 0);
      

                let stateModel = new JSONModel({
                  firstNameState: "None",
                  lastNameState: "None",
                  cityState: "None",
                  phoneNumberState: "None",
                  countyState: "None"
                });
                this.getView().setModel(stateModel, "stateModel");

                let deleteAccount = new JSONModel({
                  chechBox1: false,
                  checkBox2: false,
                  checkBox3: false,
                  suggestion:""
                });
                this.getView().setModel(deleteAccount, "deleteAccount");

                
            },

            getListings: async function () {
                let resArray = [];
                const userId = this.getUserId();

                const data = await this.get("http://localhost:3000/user/" + userId);
                if (data.listedAds.length > 0) {
                    data.listedAds.forEach(async (element) => {
                        this.byId("listingsGridList").setVisible(true);
                        this.byId("listingsText").setVisible(true);

                        let listLength = data.listedAds.length;
                        console.log(listLength);
                        const response = await this.get(
                            "http://localhost:3000/room/" + element
                        );
                        if (response) {
                            resArray.push(response);
                            if (listLength === resArray.length) {
                                this.getOwnerComponent()
                                    .getModel("listedRooms")
                                    .setData(resArray);
                            }
                        } else {
                            console.log("err");
                        }
                    });
                } else {
                    this.getOwnerComponent().getModel("listedRooms").setData({});
                }
            },

            onPressDeletePost: function (oEvent) {
                let selectedRoom = oEvent
                    .getSource()
                    .getBindingContext("listedRooms")
                    .getObject();
                this.getView().getModel("selectedRoom").setData(selectedRoom);
                let oView = this.getView();
                if (!this.byId("deleteAdDialog")) {
                    Fragment.load({
                        id: oView.getId(),
                        name: "ui5boilerplate.view.DeleteAdDialog",
                        controller: this,
                    }).then(function (oDialog) {
                        oView.addDependent(oDialog);
                        oDialog.open();
                    });
                } else {
                    this.byId("deleteAdDialog").open();
                }
            },

            handleYesDelete: function () {
                let id = this.getView().getModel("selectedRoom").getData()._id;
                this.delete("http://localhost:3000/room/" + id)
                    .then((response) => {
                        this.getListings();
                        this.byId("deleteAdDialog").close();
                        MessageToast.show(this.getI18nMessage("DELETE_POST"))
                    })
                    .catch((err) => {
                        console.log(err);
                    });
            },

            handleCancelDeleteListings: function () {
                this.byId("deleteAdDialog").close();
            },

            onPressEditPost: function(oEvent){
                let selectedRoom = oEvent
                    .getSource()
                    .getBindingContext("listedRooms")
                    .getObject();
                this.getView().getModel("selectedRoom").setData(selectedRoom);

                let id = this.getView().getModel("selectedRoom").getData()._id;

                this.get(
                    "http://localhost:3000/room/" + id
                  )
                    .then((response) => {
                      this.getView().getModel("selectedRoom").setData(response);
                      let uploadDate = this.getView()
                        .getModel("selectedRoom")
                        .getData()
                    })
                    .catch((err) => {
                      console.log(err);
                    });

                let mView = this.getView();
                if (!this.byId("EditAdDialog")) {
                    Fragment.load({
                        id: mView.getId(),
                        name: "ui5boilerplate.view.EditAdDialog",
                        controller: this,
                    }).then(function (kDialog) {
                        mView.addDependent(kDialog);
                        kDialog.open();
                    });
                } else {
                    this.byId("EditAdDialog").open();
                }
            },

            onPressSaveAd: function()
            {
                let aView = this.getView();
                if (!this.byId("EditAdConfirmationDialog")) {
                    Fragment.load({
                        id: aView.getId(),
                        name: "ui5boilerplate.view.EditAdConfirmationDialog",
                        controller: this,
                    }).then(function (xDialog) {
                        aView.addDependent(xDialog);
                        xDialog.open();
                    });
                } else {
                    this.byId("EditAdConfirmationDialog").open();
                }
            },

            handleCancelEdit: function () {
                this.byId("EditAdConfirmationDialog").close();
            },

            checkTitle: function () {
                let roomCheck = this.getView().getModel('selectedRoom').getData();
                let title = roomCheck.title;
                if (Validations.validateNoSpace(title)) {
                  this.getView().getModel('selectedRoom').setProperty('/titleState', 'None');
                } else {
                  this.getView().getModel('selectedRoom').setProperty('/titleState', 'Error');
                }
              },

              checkStreet: function () {
                let roomCheck = this.getView().getModel('selectedRoom').getData();
                let street = roomCheck.street;
                if (Validations.validateNoSpace(street)) {
                  this.getView().getModel('selectedRoom').setProperty('/streetState', 'None');
                } else {
                  this.getView().getModel('selectedRoom').setProperty('/streetState', 'Error');
                }
              },

              checkNrApt: function () {
                let roomCheck = this.getView().getModel('selectedRoom').getData();
                let nrApt = roomCheck.nrApt;
                if (Validations.validateNumber(nrApt) && nrApt) {
                  this.getView().getModel('selectedRoom').setProperty('/nrAptState', 'None');
                } else {
                  this.getView().getModel('selectedRoom').setProperty('/nrAptState', 'Error');
                }
              },

              checkCity: function () {
                let roomCheck = this.getView().getModel('selectedRoom').getData();
                let city = roomCheck.city;
                if (Validations.validateName(city)) {
                  this.getView().getModel('selectedRoom').setProperty('/cityState', 'None');
                } else {
                  this.getView().getModel('selectedRoom').setProperty('/cityState', 'Error');
                }
              },

              checkState: function () {
                let roomCheck = this.getView().getModel('selectedRoom').getData();
                let state = roomCheck.state;
                if (Validations.validateName(state)) {
                  this.getView().getModel('selectedRoom').setProperty('/stateState', 'None');
                } else {
                  this.getView().getModel('selectedRoom').setProperty('/stateState', 'Error');
                }
              },

              checkZipCode: function () {
                let roomCheck = this.getView().getModel('selectedRoom').getData();
                let zipCode = roomCheck.zipCode;
                if (Validations.validateNumber(zipCode) && zipCode) {
                  this.getView().getModel('selectedRoom').setProperty('/zipCodeState', 'None');
                } else {
                  this.getView().getModel('selectedRoom').setProperty('/zipCodeState', 'Error');
                }
              },

              checkDescription: function () {
                let roomCheck = this.getView().getModel('selectedRoom').getData();
                let description = roomCheck.description;
                if (Validations.validateNoSpace(description)) {
                  this.getView().getModel('selectedRoom').setProperty('/descriptionState', 'None');
                } else {
                  this.getView().getModel('selectedRoom').setProperty('/descriptionState', 'Error');
                }
              },

              checkOwnerDescription: function () {
                let roomCheck = this.getView().getModel('selectedRoom').getData();
                let ownerDescription = roomCheck.ownerDescription;
                if (Validations.validateNoSpace(ownerDescription)) {
                  this.getView().getModel('selectedRoom').setProperty('/ownerDescriptionState', 'None');
                } else {
                  this.getView().getModel('selectedRoom').setProperty('/ownerDescriptionState', 'Error');
                }
              },

              checkRules: function () {
                let roomCheck = this.getView().getModel('selectedRoom').getData();
                let rules = roomCheck.rules;
                if (Validations.validateNoSpace(rules)) {
                  this.getView().getModel('selectedRoom').setProperty('/rulesState', 'None');
                } else {
                  this.getView().getModel('selectedRoom').setProperty('/rulesState', 'Error');
                }
              },

              checkPrice: function () {
                let roomCheck = this.getView().getModel('selectedRoom').getData();
                let price = roomCheck.price;
                if (Validations.validateNumber(price) && price) {
                  this.getView().getModel('selectedRoom').setProperty('/priceState', 'None');
                } else {
                  this.getView().getModel('selectedRoom').setProperty('/priceState', 'Error');
                }
              },
        

            handleYesEditAd: function () { 

            let id = this.getView().getModel("selectedRoom").getData()._id;
            let data = this.getView().getModel("selectedRoom").getData();
            const title = data.title;
            const street = data.street;
            const nrApt = data.nrApt;
            const city = data.city;
            const state = data.state;
            const zipCode = data.zipCode;
            const description = data.description;
            const ownerDescription = data.ownerDescription;
            const rules =  data.rules;
            const price = data.price;
          
            if(Validations.validateNoSpace(title) && Validations.validateNoSpace(street) && Validations.validateNumber(nrApt) && Validations.validateName(city) && nrApt && Validations.validateName(state) && Validations.validateNumber(zipCode) && zipCode && Validations.validateNoSpace(description) && Validations.validateNoSpace(ownerDescription) && Validations.validateNoSpace(rules) && Validations.validateNumber(price) && price)
            {
          this.put(
            "http://localhost:3000/room/updateAd/" + id, data
          )
            .then((updateData) => {
              MessageToast.show(this.getI18nMessage("MESSAGEBOXUPDATED"));
              this.getView().getModel("selectedRoom").setData(updateData);
              this.byId('EditAdConfirmationDialog').close();
              this.byId("EditAdDialog").close();
              this.getListings();
            })
            .catch((err) => {
              console.log(err);
            });
        
                this.getView().getModel("selectedRoom").setProperty("/titleState", "None");
                this.getView().getModel("selectedRoom").setProperty("/streetState", "None");
                this.getView().getModel("selectedRoom").setProperty("/nrAptState", "None");
                this.getView().getModel("selectedRoom").setProperty("/cityState", "None");
                this.getView().getModel("selectedRoom").setProperty("/stateState", "None");
                this.getView().getModel("selectedRoom").setProperty("/zipCodeState", "None");
                this.getView().getModel("selectedRoom").setProperty("/descriptionState", "None");
                this.getView().getModel("selectedRoom").setProperty("/ownerDescriptionState", "None");
                this.getView().getModel("selectedRoom").setProperty("/rulesState", "None");
                this.getView().getModel("selectedRoom").setProperty("/priceState", "None");

                
            }
            else{
                sap.m.MessageBox.error(this.getI18nMessage("HIGHLIGHTED_FIELDS"));
          if (!Validations.validateNoSpace(title) || !title) {
            this.getView().getModel("selectedRoom").setProperty("/titleState", "Error");
          }

          if(!Validations.validateNoSpace(street) || !street){
            this.getView().getModel("selectedRoom").setProperty("/streetState", "Error");
          }

          if(!Validations.validateNumber(nrApt) || !nrApt){
            this.getView().getModel("selectedRoom").setProperty("/nrAptState", "Error");
          }

          if(!Validations.validateName(city) || !city){
            this.getView().getModel("selectedRoom").setProperty("/cityState", "Error");
          }

          if(!Validations.validateName(state)){
            this.getView().getModel("selectedRoom").setProperty("/stateState", "Error");
          }

          if(!Validations.validateNumber(zipCode) || !zipCode){
            this.getView().getModel("selectedRoom").setProperty("/zipCodeState", "Error");
          }

          if(!Validations.validateNoSpace(description)){
            this.getView().getModel("selectedRoom").setProperty("/descriptionState", "Error");
          }

          if(!Validations.validateNoSpace(ownerDescription)){
            this.getView().getModel("selectedRoom").setProperty("/ownerDescriptionState", "Error");
          }

          if(!Validations.validateNoSpace(rules)){
            this.getView().getModel("selectedRoom").setProperty("/rulesState", "Error");
          }

          if(!Validations.validateNumber(price) || !price){
            this.getView().getModel("selectedRoom").setProperty("/priceState", "Error");
          }
            }
            }
        });
    }
);
