sap.ui.define(
    [
      'ui5boilerplate/controller/Base.controller',
      'sap/ui/model/json/JSONModel',
      'sap/m/MessageToast',
      'ui5boilerplate/utils/Validations',
      'sap/m/MessageBox',
      "sap/ui/model/Filter",
      "sap/ui/model/FilterOperator"
    ],
    (BaseController, JSONModel, MessageToast, Validations, MessageBox, Filter, FilterOperator) => {
      return BaseController.extend('ui5boilerplate.controller.Top3', {
        onInit: function () {
          const oRouter = this.getOwnerComponent().getRouter();
          oRouter.getRoute("Top3").attachMatched(this.onRouteMatched, this);

          this._filters = {};

          
        },
  
        onRouteMatched: async function () {
          this.verifyToken();
          this.showBusyIndicator(2000, 0);
          this.onShowAll();
          this.onClearFilters();
          
          let stateModel = new JSONModel({
            firstNameState: "None",
            lastNameState: "None",
            cityState: "None",
            phoneNumberState: "None",
            countyState: "None",
          });
          this.getView().setModel(stateModel, "stateModel");

          let deleteAccount = new JSONModel({
            chechBox1: false,
            checkBox2: false,
            checkBox3: false,
            suggestion:""
          });
          this.getView().setModel(deleteAccount, "deleteAccount");


          let resArray = [];
        const data = await this.get("http://localhost:3000/userr/favourites")
          .then((data) => {
            this.byId("favGridList5").setVisible(true);
            this.byId("favouritesText5").setVisible(true);
            if (data.length > 0) {
              resArray = [...data];
              this.getOwnerComponent()
                .getModel("favouriteRooms")
                .setData(resArray);
            } else {
              this.getOwnerComponent().getModel("favouriteRooms").setData({});
            }
          })
          .catch((err) => {
            console.log(err);
          });
        },

        onShowAll() {
          this.byId("adsGridListTop").setVisible(true);
          this.get(
            "http://localhost:3000/roo",
          )
            .then((data) => {
              let roomAds = new JSONModel(data);
              this.getView().setModel(roomAds, "roomAds");
              console.log(roomAds);
            })
            .catch((err) => {
              console.log(err);
            });
        },

        onPressSeeMoreFav10: function (oEvent) {
          let selectedRoom = oEvent
            .getSource()
            .getBindingContext("favouriteRooms")
            .getObject();
          this.getView().getModel("selectedRoom").setData(selectedRoom);
  
          this.get(
            "http://localhost:3000/room/" +
            this.getView().getModel("selectedRoom").getData()._id
          )
  
            .then((response) => {
              let selectedRoomId = this.getView()
                .getModel("selectedRoom")
                .getData()._id;
  
              this.getRouter().navTo("RoomPage", { selectedId: selectedRoomId });
            })
  
            .catch((err) => {
              console.log(err);
            });
        },

        onCountryChange2: function (oEvent) {
          const selectedCountry = oEvent.getSource().getValue();
          console.log(selectedCountry);
          this._filters.country = new Filter("countryRegion", FilterOperator.Contains, selectedCountry);

          this.applyFiltersToTable();

          this.byId("adsGridListTop").setVisible(false);
          this.byId("favouritesText3").setVisible(false);
          this.byId("favouritesPictureTop").setVisible(false);
          this.byId("VBoxTop").setVisible(false);
        },

        onMainDescriptionChange: function (oEvent) {
          const selectedDescription = oEvent.getSource().getValue();
          console.log(selectedDescription);
          this._filters.mainDescription = new Filter("mainDescription", FilterOperator.Contains, selectedDescription);
          this.applyFiltersToTable();

          this.byId("adsGridListTop").setVisible(false);
          this.byId("favouritesText3").setVisible(false);
          this.byId("favouritesPictureTop").setVisible(false);
          this.byId("VBoxTop").setVisible(false);
        },
    
        onTypeRoomChange: function (oEvent) {
          const selectedTypeRoom = oEvent.getSource().getValue();
          console.log(selectedTypeRoom);
          this._filters.mainDescription = new Filter("typeRoom", FilterOperator.Contains, selectedTypeRoom);
          this.applyFiltersToTable();

          this.byId("adsGridListTop").setVisible(false);
          this.byId("favouritesText3").setVisible(false);
          this.byId("favouritesPictureTop").setVisible(false);
          this.byId("VBoxTop").setVisible(false);
        },
    
        onBedroomsChange: function (oEvent) {
          const selectedBedrooms = oEvent.getSource().getValue();
          console.log(selectedBedrooms);
          this._filters.bedrooms = new Filter("bedrooms", FilterOperator.Contains, selectedBedrooms);
          this.applyFiltersToTable();

          this.byId("adsGridListTop").setVisible(false);
          this.byId("favouritesText3").setVisible(false);
          this.byId("favouritesPictureTop").setVisible(false);
          this.byId("VBoxTop").setVisible(false);
        },
    
        onBedsChange: function (oEvent) {
          const selectedBeds = oEvent.getSource().getValue();
          console.log(selectedBeds);
          this._filters.beds = new Filter("beds", FilterOperator.Contains, selectedBeds);
          this.applyFiltersToTable();

          this.byId("adsGridListTop").setVisible(false);
          this.byId("favouritesText3").setVisible(false);
          this.byId("favouritesPictureTop").setVisible(false);
          this.byId("VBoxTop").setVisible(false);
        },
    
        onBathroomsChange: function (oEvent) {
          const selectedBathrooms = oEvent.getSource().getValue();
          console.log(selectedBathrooms);
          this._filters.bathrooms = new Filter("bathrooms", FilterOperator.Contains, selectedBathrooms);
          this.applyFiltersToTable();

          this.byId("adsGridListTop").setVisible(false);
          this.byId("favouriteText3").setVisible(false);
          this.byId("favouritesPictureTop").setVisible(false);
          this.byId("VBoxTop").setVisible(false);
        },
    
        onPriceMinChange: function (oEvent) {
          const selectedPriceMin = oEvent.getSource().getValue();
          console.log(selectedPriceMin);
          this._filters.priceMin = new Filter("price", FilterOperator.GE, selectedPriceMin);
          this.applyFiltersToTable();

          this.byId("adsGridListTop").setVisible(false);
          this.byId("favouritesText3").setVisible(false);
          this.byId("favouritesPictureTop").setVisible(false);
          this.byId("VBoxTop").setVisible(false);
        },
    
        onPriceMaxChange: function (oEvent) {
          const selectedPriceMax = oEvent.getSource().getValue();
          console.log(selectedPriceMax);
          this._filters.priceMax = new Filter("price", FilterOperator.LE, selectedPriceMax);
          this.applyFiltersToTable();

          this.byId("adsGridListTop").setVisible(false);
          this.byId("favouritesText3").setVisible(false);
          this.byId("favouritesPictureTop").setVisible(false);
          this.byId("VBoxTop").setVisible(false);
    
          if (selectedPriceMax == "") {
            this.onShowAll();
            this._filters = {};
          }
        },

        applyFiltersToTable: function () {
          const dataTable = this.getView().byId("adsGridListTop");
          this.aFilters = [];
          for (const property in this._filters) {
            this.aFilters.push(this._filters[property])
          }
          const filters = new Filter({
            aFilters: this.aFilters,
            bAnd: true
          })
          dataTable.getBinding("items").filter(filters);
          console.log(this.byId("adsGridListTop"));
        },

        onClearFilters() {
    
          this.byId("countryRegionCombo1").clearSelection();
          this.byId("mainDescriptionCombo1").clearSelection();
          this.byId("typeRoomCombo1").clearSelection();
          this.byId("bedroomsCombo1").clearSelection();
          this.byId("bedsCombo1").clearSelection();
          this.byId("bathroomsCombo1").clearSelection();
          this.byId("priceMinCombo1").setValue("");
          this.byId("priceMaxCombo1").setValue("");
    
          this.getOwnerComponent().getModel("filteredBy").setData({});

          this.byId("adsGridListTop").setVisible(false);
          this.byId("favouritesText3").setVisible(false);
          this.byId("favouritesPictureTop").setVisible(false);
          this.byId("VBoxTop").setVisible(false);
        },

        onApplyPressTop() {
          var maxItems=3;
          var gridlistItems = this.getView().byId("adsGridListTop");
          var items = gridlistItems.getItems();
        
          for(var i = 0; i<items.length; i++)
          {
            if(i<maxItems){
            items[i].setVisible(true);
            } else{
              items[i].setVisible(false);
            }
          }

          console.log(items);
          this.byId("adsGridListTop").setVisible(true);
          this.byId("favouritesText3").setVisible(true);
          this.byId("favouritesPictureTop").setVisible(true);
          this.byId("VBoxTop").setVisible(true);

          this.byId("topPage").scrollTo(0);
        }
  
      });
    }
  );
  