sap.ui.define(
    [
      'ui5boilerplate/controller/Base.controller',
      'sap/ui/model/json/JSONModel',
      
    ],
    (BaseController, JSONModel) => {
      return BaseController.extend('ui5boilerplate.controller.AboutUs', {
        onInit: function () {
          const oRouter = this.getOwnerComponent().getRouter();
          oRouter.getRoute("AboutUs").attachMatched(this.onRouteMatched, this);
        },
  
        onRouteMatched: function () {
          this.verifyToken();
          this.showBusyIndicator(2000, 0);

          let stateModel = new JSONModel({
            firstNameState: "None",
            lastNameState: "None",
            cityState: "None",
            phoneNumberState: "None",
            countyState: "None"
          });
          this.getView().setModel(stateModel, "stateModel");

          let deleteAccount = new JSONModel({
            chechBox1: false,
            checkBox2: false,
            checkBox3: false,
            suggestion:""
          });
          this.getView().setModel(deleteAccount, "deleteAccount");


        },

        onAfterRendering: function () {
          this.onChangePage();
          this.onChangePage1();
        },
  
        onChangePage: function () {
          var oCarousel = this.byId('aboutUsCarousel');
          setTimeout(function () {
            oCarousel.next();
          }, 4500);
        },

        onChangePage1: function(){
          var oCarousel = this.byId('aboutUsCarousel1');
          setTimeout(function () {
            oCarousel.next();
          }, 4500);
        },

        onPressSvalbard: function (url, newTab) {
          sap.ui.require(["sap/m/library"], ({ URLHelper }) =>
            URLHelper.redirect("http://localhost:8080/#/RoomPage/645371315d071654aef3b36f", true)
          );
        },

        onPressKotor: function (url, newTab) {
          sap.ui.require(["sap/m/library"], ({ URLHelper }) =>
            URLHelper.redirect("http://localhost:8080/#/RoomPage/6453a6d45d071654aef3c2b6", true)
          );
        },
      });
    }
  );