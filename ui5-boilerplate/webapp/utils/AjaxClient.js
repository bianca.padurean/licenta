sap.ui.define(['jquery.sap.global'], function (jQuery) {
  'use strict';

  return {
    get: (url) => {
      const that = this;
      return new Promise(function (resolve, reject) {
        jQuery.ajax({
          url: url,
          type: 'GET',
          xhrFields: {
            withCredentials: true,
          },
          success: (data) => {
            resolve(data);
          },
          error: (err) => {
            reject(err);
          },
        });
      });
    },

    post: (url, data) => {
      return new Promise(function (resolve, reject) {
        jQuery.ajax({
          url: url,
          type: 'POST',
          data: data,
          xhrFields: {
            withCredentials: true,
          },
          success: (response) => {
            resolve(response);
          },
          error: (err) => {
            reject(err);
          },
        });
      });
    },

    put: function (url, data) {
      return new Promise(function (resolve, reject) {
        jQuery.ajax({
          url: url,
          type: 'PUT',
          data: data,
          xhrFields: {
            withCredentials: true,
          },
          success: (response) => {
            resolve(response);
          },
          error: (err) => {
            reject(err);
          },
        });
      });
    },

    delete: function (url) {
      return new Promise(function (resolve, reject) {
        jQuery.ajax({
          url: url,
          type: 'DELETE',
          xhrFields: {
            withCredentials: true,
          },
          success: (data) => {
            resolve(data);
          },
          error: (err) => {
            reject(err);
          },
        });
      });
    },
  };
});
