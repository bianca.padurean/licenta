sap.ui.define(['sap/ui/core/ComponentContainer'], function (ComponentContainer) {
  'use strict';

  new ComponentContainer({
    name: 'ui5boilerplate',
    settings: {
      id: 'ui5bloilerplate',
    },
    async: true,
  }).placeAt('content');
});
