sap.ui.define(['sap/ui/core/UIComponent', "sap/ui/model/json/JSONModel"], (UIComponent, JSONModel) => {
  return UIComponent.extend('ui5boilerplate.Component', {
    metadata: {
      manifest: 'json',
    },

    /**
     * The component is initialized by UI5 automatically during the
     * startup of the app and calls the init method once.
     * @public
     * @override
     */
    init() {
      let filteredBy = new JSONModel();
      this.setModel(filteredBy, "filteredBy");

      let selectedRoom = new JSONModel();
      this.setModel(selectedRoom, "selectedRoom");

      let amenitiesModel = new JSONModel();
      this.setModel(amenitiesModel, "amenitiesModel");

      // Initialize UI component
      // eslint-disable-next-line prefer-rest-params
      UIComponent.prototype.init.apply(this, arguments);

      // create the views based on the url/hash
      this.getRouter().initialize();

      //call the chatbot function
      this.getChatBot();
    },

    getChatBot: function () {
      //create the script tag as given in the global settings
      if (!document.getElementById("cai-webchat")) {
        var s = document.createElement("script");
        s.setAttribute("id", "cai-webchat");
        s.setAttribute("src", "https://cdn.cai.tools.sap/webchat/webchat.js");
        document.body.appendChild(s);
      }
      s.setAttribute("channelId", "8eb07d55-7b6f-46e8-ac62-6a775bcf3b8b");
      s.setAttribute("token", "8e6d4a43c9424f7bef595be1d3c26b72");
    }
  });
});
