const casual = require("casual");

function generateSignInData(requestParams, ctx, ee, next){
  ctx.vars["email"] = casual.email;
  ctx.vars["password"] = casual.password;

  return next();
}

module.exports = {
    generateSignInData,
  };