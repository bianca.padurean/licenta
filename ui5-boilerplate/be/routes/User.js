import jwt from 'jsonwebtoken';
import UserModel from '../models/user.js';
import bcrypt from 'bcryptjs';
import RoomAdModel from "../models/roomAd.js";
import CryptoJS from 'crypto-js';
import nodemailer from 'nodemailer';

function validatePhone(phone) {
  const regex = /^\+?\d{10}$/;
  return regex.test(phone);
}

function validateEmail(email) {
  const regex = /^\w+[\w-+\.]*\@\w+([-\.]\w+)*\.[a-zA-Z]{2,}$/;
  return regex.test(email);
}

function validateUser(user) {
  const keys = Object.keys(user);
  for (let i = 0; i < keys.length; i++) {
    if (typeof user[keys[i]] === 'string') {
      if (!/\S/.test(user[keys[i]]) || user[keys[i]] === '') {
        return false;
      }
    }
  }
  return true;
}

export async function register(req, res, next) {
  try {
    console.log(req.body);
    req.body.password = CryptoJS.AES.decrypt(
      req.body.password,
      process.env.ENCRYPTION_SECRET
    ).toString(CryptoJS.enc.Utf8);

    const userData = {
      firstName: req.body.firstName,
      lastName: req.body.lastName,
      email: req.body.email,
      password: req.body.password,
      confirm: req.body.confirm_password,
      city: req.body.city,
      countryRegion: req.body.countryRegion,
      phoneNumber: req.body.phoneNumber,
      status: req.body.status,
      favourites: [],
      listedAds: [],
    };

    if (validatePhone(userData.phoneNumber) === false) {
      res.status(400).json({ error: 'Eroare: Numar de telefon invalid' });
      return;
    }

    if (validateEmail(userData.email) === false) {
      res.status(400).json({ error: 'Eroare: Adresa de mail invalida' });
      return;
    }

    if (validateUser(userData) === false) {
      res.status(400).json({ error: 'Eroare: Unele campuri nu sunt completate' });
      return;
    }

    if (userData.confirm !== userData.password) {
      res.status(400).json({ error: "Eroare: Campurile pentru parola nu se potrivesc" });
      return;
    }

    delete userData.confirm;

    const user = await UserModel.findOne({ email: userData.email });
    if (!user) {
      const hash = bcrypt.hashSync(req.body.password, 10);

      if (hash) {
        userData.password = hash;

        const response = await UserModel.create(userData);

        if (response) {
          res.status(201).json({ email: response.email, _id: response._id });
        }
      }
    } else {
      res.status(409).json({ error: `Un cont cu adresa de email ${userData.email} exista deja.` });
    }
  } catch (err) {
    next(err);
  }
}

export async function login(req, res, next) {
  console.log(req.body);
  try {
    req.body.password = CryptoJS.AES.decrypt(
      req.body.password,
      process.env.ENCRYPTION_SECRET
    ).toString(CryptoJS.enc.Utf8);

    console.log(req.body);

    const user = await UserModel.findOne({ email: req.body.email });

    if (user) {
      if (bcrypt.compareSync(req.body.password, user.password)) {
        console.log(req.body.password);
        console.log(user.password);
        const token = jwt.sign(user.email, process.env.ACCESS_TOKEN_SECRET, {});

        res.cookie('token', token, { sameSite: 'None', secure: true });
        res.cookie('id', user.id, { sameSite: 'None', secure: true });
        res.status(200).json({
          _id: user.id,
          token: token,
        });
      } else {
        res.status(401).end();
      }
    } else {
      res.status(401).end();
    }
  } catch (err) {
    next(err);
  }
}

export async function auth(req, res, next) {
  console.log(req.cookies);
  try {
    const data = await UserModel.findById(req.cookies.id);

    if (data) {
      const decoded = jwt.verify(req.cookies.token, process.env.ACCESS_TOKEN_SECRET);

      if (decoded) {
        res.status(200).json(decoded);
      } else {
        res.status(401).end();
      }
    } else {
      res.status(404).json({ message: `User with id ${req.body._id} not found` });
    }
  } catch (err) {
    next(err);
  }
}

export function user(req, res, next) {
  console.log(req.cookies);
  console.log(process.env.ACCESS_TOKEN_SECRET);
  const auth = jwt.verify(req.cookies.token, process.env.ACCESS_TOKEN_SECRET);
  if (auth) {
    const id = req.params.id;
    UserModel.findById(id)
      .then((result) => {
        if (result) {
          res.status(200).send(result);
        } else {
          res.status(404).end();
        }
      })
      .catch((err) => {
        next(err);
      });
  } else {
    res.status(401).end();
  }
}

export function update(req, res, next) {
  const auth = jwt.verify(req.cookies.token, process.env.ACCESS_TOKEN_SECRET);

  if (auth) {
    const id = req.cookies.id;

    if (validatePhone(req.body.phoneNumber) === false) {
      res.status(400).json({ error: 'Error: Invalid phone number.' });
      return;
    }

    if (validateUser(req.body) === false) {
      res.status(400).json({ error: 'Error: Some fields are not filled in.' });
      return;
    }

    if (validateEmail(req.body.email) === false) {
      res.status(400).json({ error: 'Error: Invalid email address.' });
      return;
    }

    if (typeof req.body.city != 'string') {
      next(new TypeError('Error: Incorrect type for city.'));
    }

    if (typeof req.body.firstName != 'string') {
      next(new TypeError('Error: Incorrect type for first name.'));
    }

    if (typeof req.body.lastName != 'string') {
      next(new TypeError('Error: Incorrect type for last name.'));
    }

    UserModel.findByIdAndUpdate(id, req.body)
      .then((result) => {
        if (result) {
          res.status(201).json(result);
        }
      })
      .catch((err) => {
        next(err);
      });
  } else {
    res.status(401).end();
  }
}

export function deleteUser(req, res, next) {
  const auth = jwt.verify(req.cookies.token, process.env.ACCESS_TOKEN_SECRET);

  if (auth) {
    const id = req.cookies.id;

    UserModel.findByIdAndRemove(id)
      .then((result) => {
        if (result) {
          res.status(201).json(result);
        }
      })
      .catch((err) => {
        next(err);
      });
  } else {
    res.status(401).end();
  }
}

export function getFavouritesFromUser(req, res, next) {
  const auth = jwt.verify(req.cookies.token, process.env.ACCESS_TOKEN_SECRET);
  if (auth) {
    const id = req.cookies.id;
    UserModel.findById(id)
      .then(async (data) => {
        if (data) {
          for (let index = 0; index < data.favourites.length; index++) {
            const room = await RoomAdModel.findById(data.favourites[index]);
            if (!room) {
              data.favourites.splice(index, 1);
              index--;
            }
          }
          await data.save();
          data = await data.populate("favourites");
          res.status(200).json(data.favourites);
        }
      })
      .catch((err) => {
        next(err);
      });
  }
}