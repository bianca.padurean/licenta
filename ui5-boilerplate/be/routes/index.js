import express from 'express';
const router = express.Router();
import { GridFsStorage } from "multer-gridfs-storage";
import { register, login, auth, user, update, deleteUser, getFavouritesFromUser } from './User.js';
import { image, postImages, deleteImage } from "./Images.js";
import { room, addToFavourites, findRoomById, deleteRoom, removeFavourites, roomsGet, rate, deleteRoom2, updateRoom} from "./RoomAd.js";
import dotenv from 'dotenv';
import multer from 'multer';
import jwt from 'jsonwebtoken';
import { uuid } from 'uuidv4';
import { get } from 'http';

dotenv.config();

export const imgStorage = new GridFsStorage({
  url: `mongodb+srv://${process.env.DB_USERNAME}:${process.env.DB_PASSWORD}@cluster0.xlof9sp.mongodb.net/test`,
  file: (req, file) => {
    return {
      filename: uuid(),
      bucketName: "images",
    };
  },
});

const imgUpload = await multer({
  storage: imgStorage,
  //Validate file MIME types
  fileFilter: function (req, file, cb) {
    if (
      file.mimetype === "image/jpeg" ||
      file.mimetype === "image/png" ||
      file.mimetype === "image/svg+xml" ||
      file.mimetype === "image/webp"
    ) {
      return cb(null, true);
    } else {
      return cb(
        new TypeError(`MIME type '${file.mimetype}' is not acceptable.`)
      );
    }
  },

  // Limit the ammount of uploaded images to at most 5 per post
  limits: {
    files: 5,
  },
});

//user routes
router.route('/register').post(register);
router.route('/login').post(login);
router.route('/auth').post(auth);
router.route('/user/:id').get(user); //get user by id
router.route('/user/update/:id').put(update);
router.route('/user/deleteUser/:id').delete(deleteUser);
router.route("/userr/favourites").get(getFavouritesFromUser);

router.route("/image/:imgID").get(image);
router.route("/images").post(
  function (req, res, next) {
    try {
      const auth = jwt.verify(
        req.cookies.token,
        process.env.ACCESS_TOKEN_SECRET
      );

      if (auth) {
        // If the request is authorized, then the next function (imgUpload) is called
        next();
      } else {
        res.status(401).end();
      }
    } catch (err) {
      next(err);
    }
  },
  imgUpload.array("files[]"),
  postImages
);
router.route("/image/:imgID").delete(deleteImage);

//room routes
router.route("/room").post(room);
router.route("/room/:id").get(findRoomById);
router.route("/add-to-favourites/:id").post(addToFavourites);
router.route("/room/:id").delete(deleteRoom);
router.route("/favourites/:id").delete(removeFavourites);
router.route("/roo").get(roomsGet);
router.route("/fav/:id").delete(removeFavourites);
router.route("/rate").post(rate);
router.route("/roomm/:id").delete(deleteRoom2);
router.route("/room/updateAd/:id").put(updateRoom);

export default router;