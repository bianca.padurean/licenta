import RoomAdModel from "../models/roomAd.js";
import UserModel from "../models/user.js";
import jwt from "jsonwebtoken";
import mongoose from "mongoose";
import ImageModel from "../models/image.js";

const imgGFS = new mongoose.mongo.GridFSBucket(mongoose.connection, {
  bucketName: "images",
});

export async function room(req, res, next) {
  try {
    const auth = jwt.verify(req.cookies.token, process.env.ACCESS_TOKEN_SECRET);

    if (auth) {
      const user = await UserModel.findById(req.body.owner);

      if (user) {
        const newRoom = new RoomAdModel({
          title: req.body.title,
          mainDescription: req.body.mainDescription,
          typeRoom: req.body.typeRoom,
          street: req.body.street,
          nrApt: req.body.nrApt,
          city: req.body.city,
          state: req.body.state,
          zipCode: req.body.zipCode,
          countryRegion: req.body.countryRegion,
          bedrooms: req.body.bedrooms,
          bathrooms: req.body.bathrooms,
          beds: req.body.beds,
          wifi: req.body.wifi,
          tv: req.body.tv,
          kitchen: req.body.kitchen,
          washer: req.body.washer,
          freeParking: req.body.freeParking,
          airConditioning: req.body.airConditioning,
          pool: req.body.pool,
          exerciseEquipment: req.body.exerciseEquipment,
          smokeAlarm: req.body.smokeAlarm,
          firstAidKit: req.body.firstAidKit,
          fireExtinguisher: req.body.fireExtinguisher,
          description: req.body.description,
          securityCamera: req.body.securityCamera,
          price: req.body.price,
          ownerDescription: req.body.ownerDescription,
          rules: req.body.rules,
          owner: user._id,
          sumRatings: req.body.sumRatings,
          totalRatings: req.body.totalRatings,
          averageRatings: req.body.averageRatings,
          images: req.body.images,
          comments: req.body.comments, 
          xLabel: req.body.xLabel,
          yLabel: req.body.yLabel,
        });

        const response = await newRoom.save();

        if (response) {
          user.listedAds.push(newRoom._id);
          const userResponse = user.save();

          if (userResponse) {
            res.status(201).json(response);
          }
        }
      } else {
        res.status(400).json({ error: `User ${req.body.id} does not exist` });
      }
    } else {
      res.status(401).end();
    }

  } catch (err) {
    next(err);
  }
}

export async function findRoomById(req, res, next) {
  try {
    const auth = jwt.verify(req.cookies.token, process.env.ACCESS_TOKEN_SECRET);

    if (auth) {
      const data = await RoomAdModel.findById(req.params.id);

      if (data) {
        res.status(200).json(data);
      }
      else {
        res.status(404).end();
      }
    } else {
      res.status(401).end();
    }
  } catch (err) {
    next(err);
  }
}

export function roomsGet(req, res) {
  try {
    RoomAdModel.find()
      .then((result) => {
        if (result) {
          res.status(200).send(result);
        } else {
          res.status(404).end();
        }
      }).catch((err) => {
        res.status(401).end();
      })
  } catch (err) {
    res.status(500).json({ message: "Internal server error" });
  }
}

export async function addToFavourites(req, res, next) {
  try {
    const auth = jwt.verify(req.cookies.token, process.env.ACCESS_TOKEN_SECRET);

    if (auth) {
      const data = await UserModel.findById(req.cookies.id);

      if (data) {
        if (data.favourites.indexOf(req.params.id) === -1) {
          data.favourites.push(req.params.id);
          const response = await data.save();
          if (response) {
            res.status(201).json(data);
          }
        } else {
          res
            .status(409)
            .json({
              error: `Room ${req.params.id} already exists in user's favourites`,
            });
        }
      } else {
        res.status(404).end();
      }
    } else {
      res.status(401).end();
    }
  } catch (err) {
    next(err);
  }
}

export async function deleteRoom(req, res, next) {
  try {
    const auth = jwt.verify(req.cookies.token, process.env.ACCESS_TOKEN_SECRET);

    if (auth) {

      //remove the room from user's listedAds
      const user = await UserModel.findById(req.cookies.id);

      const listedArray = user.listedAds.filter((roomId) => {
        const ID = JSON.stringify(roomId).split(/[\"()]/);
        return ID[ID.length - 2] !== req.params.id;
      });

      user.listedAds = listedArray;

      //remove the room from user's favourites
      const favouritesArray = user.favourites.filter((roomId) => {
        const ID = JSON.stringify(roomId).split(/[\"()]/);
        return ID[ID.length - 2] !== req.params.id;
      });

      user.favourites = favouritesArray;
      user.save();

      const room = await RoomAdModel.findById(req.params.id);

      if (room) {
        //delete the images associated with a room
        room.images.forEach(async (imageID) => {
          const image = await ImageModel.findById(imageID);
          if (image) {
            imgGFS.delete(imageID);
          } else {
            return;
          }
        });

        const response = await room.remove();

        if (response) {
          res.status(204).end();
        }
      }
    } else {
      res.status(401).end();
    }
  } catch (err) {
    next(err);
  }
}

export async function removeFavourites(req, res, next) {
  try {
    const auth = jwt.verify(req.cookies.token, process.env.ACCESS_TOKEN_SECRET);
    if (auth) {
      const user = await UserModel.findById(req.cookies.id);
      const favouritesArray = user.favourites.filter((roomId) => {
        const ID = JSON.stringify(roomId).split(/[\"()]/);
        return ID[ID.length - 2] !== req.params.id;
      });
      user.favourites = favouritesArray;
      const response = await user.save();

      if (response) {
        res.status(200).json(response);
      }
    } else {
      res.status(401).end();
    }
  } catch (err) {
    next(err);
  }
}

export async function rate(req, res, next) {
  try {
    const auth = jwt.verify(req.cookies.token, process.env.ACCESS_TOKEN_SECRET);
    if (auth) {
      const stars = req.body.stars;
      const roomId = req.body.roomID;
      const ratedRoom = await RoomAdModel.findById(roomId); // sellerul rate uit    

      ratedRoom.totalRatings = ratedRoom.totalRatings + 1;
      ratedRoom.sumRatings = ratedRoom.sumRatings + Number(stars); //suma ratingurilor 
      ratedRoom.averageRatings = Number(ratedRoom.sumRatings / ratedRoom.totalRatings).toFixed(1);
      const response = await ratedRoom.save();
      if (response) {
        res.status(200).json(ratedRoom);
      }
    }
    else {
      res.status(401).end();
    }
  }
  catch (err) {
    next(err)
  }
}

export async function deleteRoom2(req, res, next) {
  try {
    const auth = jwt.verify(req.cookies.token, process.env.ACCESS_TOKEN_SECRET);

    if (auth) {

      //remove the room from user's listedAds
      const user = await UserModel.findById(req.cookies.id);

      const listedArray = user.listedAds.filter((roomId) => {
        const ID = JSON.stringify(roomId).split(/[\"()]/);
        return ID[ID.length - 2] !== req.params.id;
      });

      user.listedAds = listedArray;

      //remove room from user's favourites
      const favouritesArray = user.favourites.filter((roomId) => {
        const ID = JSON.stringify(roomId).split(/[\"()]/);
        return ID[ID.length - 2] !== req.params.id;
      });

      user.favourites = favouritesArray;
      
      const room = await RoomAdModel.findById(req.params.id);

      if (room) {
        //delete the images associated with a room
        room.images.forEach(async (imageID) => {
          const image = await ImageModel.findById(imageID);
          if (image) {
            imgGFS.delete(imageID);
          } else {
            return;
          }
        });

        const response = await room.remove();

        if (response) {
          res.status(204).end();
        }
      }
    } else {
      res.status(401).end();
    }
  } catch (err) {
    next(err);
  }
}

export async function updateRoom(req, res, next) {
  const auth = jwt.verify(req.cookies.token, process.env.ACCESS_TOKEN_SECRET);

  console.log(req.body);

  if (auth) {
    const objId = new mongoose.Types.ObjectId(req.params.id);
    const id = await RoomAdModel.findById(objId);
    console.log(id);
  
    RoomAdModel.findByIdAndUpdate(id.id, req.body)
      .then((result) => {
        if (result) {
          res.status(201).json(result);
        }
      })
      .catch((err) => {
        next(err);
      });
  } else {
    res.status(401).end();
  }
}