import express from 'express';
import mongoose, { mongo } from 'mongoose';
import bodyParser from 'body-parser';
import dotenv from 'dotenv';
import cors from 'cors';
import routes from './routes/index.js';
import cookieParser from 'cookie-parser';
import jwt from 'jsonwebtoken';

const app = express();
const port = process.env.PORT;

dotenv.config();

app.use(cookieParser());
app.use(express.json());
app.use(bodyParser.urlencoded({ extended: true }));
app.use(bodyParser.json());

app.use(
  // localhost and BTP URIs
  cors({
    origin: ['http://localhost:8080'],
    credentials: true,
  })
);

const mongoUri = `mongodb+srv://${process.env.DB_USERNAME}:${process.env.DB_PASSWORD}@cluster0.xlof9sp.mongodb.net/test`;

mongoose
  .connect(mongoUri, { useNewUrlParser: true })
  .then(() => console.log('MongoDB connected'))
  .catch((err) => console.log(err));

app.use('/', routes);


app.listen(port, () => {
  console.log(`Example app listening on port ${port}`);
});
