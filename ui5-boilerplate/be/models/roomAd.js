import mongoose, { Mongoose } from "mongoose";

const roomAdSchema = mongoose.Schema(
    {
        title: {
            type: String,
            required: true,
        },
        mainDescription: {
            type: String,
            required: true,
        },
        typeRoom: {
            type: String,
            required: true,
        },
        street: {
            type: String,
            required: true,
        },
        nrApt: {
            type: Number,
            required: false,
        },
        city: {
            type: String,
            required: true,
        },
        state: {
            type: String,
            required: false,
        },
        zipCode: {
            type: String,
            required: false,
        },
        countryRegion: {
            type: String,
            required: true,
        },
        bedrooms: {
            type: String,
            required: true,
        },
        bathrooms: {
            type: String,
            required: true,
        },
        beds: {
            type: String,
            required: true,
        },
        wifi: {
            type: Boolean,
            required: true,
        },
        tv: {
            type: Boolean,
            required: true,
        },
        kitchen: {
            type: Boolean,
            required: true,
        },
        washer: {
            type: Boolean,
            required: true,
        },
        freeParking: {
            type: Boolean,
            required: true,
        },
        airConditioning: {
            type: Boolean,
            required: true,
        },
        pool: {
            type: Boolean,
            required: true,
        },
        exerciseEquipment: {
            type: Boolean,
            required: true,
        },
        smokeAlarm: {
            type: Boolean,
            required: true,
        },
        firstAidKit: {
            type: Boolean,
            required: true,
        },
        fireExtinguisher: {
            type: Boolean,
            required: true,
        },
        description: {
            type: String,
            required: true,
        },
        securityCamera: {
            type: Boolean,
            required: true,
        },
        price: {
            type: Number,
            required: true,
        },
        ownerDescription: {
            type: String,
            required: true,
        },
        rules: {
            type: String,
            required: true,
        },
        owner: {
            type: mongoose.Schema.Types.ObjectId,
            ref: "UserModel",
        },
        sumRatings: {
            type: Number,
            required: false,
        },
        totalRatings: {
            type: Number,
            required: false,
        },
        averageRatings: {
            type: Number,
            required: false,
        },
        xLabel: {
            type: String,
            required: true,
        },
        yLabel: {
            type: String,
            required: true,
        },
        comments: [
            {
                reviewer: {
                    type: String,
                    required: false,
                },
                textComment: {
                    type: String,
                    required: false,
                },
            }
        ],
        images: [
            {
                type: mongoose.Schema.Types.ObjectId,
                ref: "images.files",
            },
        ],
    },
    { timestamps: true }
);

const RoomAdModel = mongoose.model("RoomAdModel", roomAdSchema);
export default RoomAdModel;