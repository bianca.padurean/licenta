import mongoose from 'mongoose';

const UserSchema = mongoose.Schema(
  {
    firstName: {
      type: String,
      required: true,
    },
    lastName: {
      type: String,
      required: true,
    },
    email: {
      type: String,
      required: true,
      unique: true,
    },
    password: {
      type: String,
      required: true,
    },
    city: {
      type: String,
      required: true,
    },
    countryRegion: {
      type: String,
      required: true,
    },
    phoneNumber: {
      type: String,
      required: true,
    },
    status:{
      type: String,
      required: true,
    },
    favourites: [
      {
        type: mongoose.Schema.Types.ObjectId,
        ref: 'RoomAdModel',
      },
    ],
    listedAds: [
      {
        type: mongoose.Schema.Types.ObjectId,
        ref: 'RoomAdModel',
      },
    ],
  },
  { timestamps: true }
);

const UserModel = mongoose.model('Users', UserSchema);
export default UserModel;
