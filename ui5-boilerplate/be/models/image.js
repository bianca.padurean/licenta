import mongoose from "mongoose";

const imageSchema = mongoose.Schema(
    {
        length: {
            type: Number,
            required: true,
        },
        chunkSize: {
            type: Number,
            required: true,
        },
        uploadDate: {
            type: Date,
            required: true,
        },
        filename: {
            type: String,
            required: true,
        },
        contentType: {
            type: String,
            required: true,
        },
    },
    { collection: "images.files" },
    { timestamps: true}
);

const ImageModel = mongoose.model("ImageModel", imageSchema);
export default ImageModel;