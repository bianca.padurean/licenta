# Proiectul de licență 

Acest proiect prezintă codul sursă pentru **Explora** - Aplicație Web destinată închirierii unei locuințe direct de la proprietar. 
Tehnologiile folosite pentru sistemul backend sunt: NodeJs, ExpressJs, Cors, JWT și Mongoose, iar pentru sistemul frontend: SAPUI5, Ajax și SAP Conversational AI.
Baza de date folosită este MongoDB.

# Pașii necesari proceselor de instalare, compilare și lansare a aplicației

**Pașii necesari procesului de instalare**

1. Clonați acest repository

2. Instalați Visual Studio Code: https://code.visualstudio.com/download

3. Instalați NodeJS: https://nodejs.org/en/download

4. `npm install`

5. `npm install --global nodemon`

6. `npm install --save multer`

7. `npm install mongoose`

8. `npm install mongocli`

9. `npm install faker`

10. `npm install express`

11. `npm install encrypt`

12. `npm install casual`

13. `npm install artillery`


**Pașii necesari proceselor de compilare și lansare**

• Comanda pentru lansarea backend: `nodemon server.js`
    - aceasta comanda va fi apelată din interiorul directorului *be*

• Comanda pentru lansarea backend: `npm start`
    - aceasta comanda va fi apelată din interiorul directorului *ui5-boilerplate*